import argparse
import string

def main():
    parser = argparse.ArgumentParser(description="Utilize a chosen plaintext attack to decrypt the vending machine password")
    
    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument("block_length", type=int, help="Cipher block length")

    subparsers = parser.add_subparsers(dest="mode")
    subparsers.add_parser("gen", parents=[parent_parser], help="Generate a password to determine the codebook")

    dec_parser = subparsers.add_parser("dec", parents=[parent_parser], help="Decrypt the ciphertext from the original config file")
    dec_parser.add_argument("orig_ciphertext", help="Ciphertext from original config file")
    dec_parser.add_argument("attack_ciphertext", help="Ciphertext from entering password generated with 'gen'")

    args = parser.parse_args()
    alphabet = string.ascii_letters + string.digits

    if args.mode == "gen":
        password_parts = []
        for c in alphabet:
            password_parts.append(c * args.block_length)
        print("".join(password_parts))
    elif args.mode == "dec":
        encrypt_codebook = dict([(x, [None] * args.block_length) for x in alphabet])
        decrypt_codebook = dict([(x, [None] * args.block_length) for x in alphabet])

        for i, plain_char in enumerate(alphabet):
            for j in range(8):
                cipher_char = args.attack_ciphertext[(i * args.block_length) + j]
                encrypt_codebook[plain_char][j] = cipher_char
                decrypt_codebook[cipher_char][j] = plain_char

        plaintext = []
        for i, c in enumerate(args.orig_ciphertext):
            plaintext.append(decrypt_codebook[c][i % args.block_length])

        print("The password is:")
        print("".join(plaintext))

if __name__ == "__main__":
    main()