import argparse
import json
import re
import sys

import mt19937predictor
import requests
import websocket               

def main():
    parser = argparse.ArgumentParser(description="Beat Snowball Fight on Impossible")
    parser.add_argument("input", type=argparse.FileType("r"), help="Discarded random values from page comments")

    args = parser.parse_args()

    print("[+] Welcome to the Snowball Fight game solver")
    print("[+] Determining the value used as the game seed")
    previous_values = re.findall(r"\d+", args.input.read())

    predictor = mt19937predictor.MT19937Predictor()

    for value in previous_values:
        predictor.setrand_int32(int(value))

    next_value = predictor.genrand_int32()

    print(f"[+] Game seed: {next_value}")
    print("[+] Playing on Easy to determine the location of the fort walls")
    data = {
        "difficulty": 0,
        "playerName": next_value,
        "playerID": "HughRansomDrysdale"
    }

    response = requests.post("https://snowball2.kringlecastle.com/game", data=data)
    cookie = response.headers["set-cookie"].split(";")[0]

    ws = websocket.create_connection("wss://snowball2.kringlecastle.com/ws", cookie=cookie)

    ws.send('{"Type":"Recon","Difficulty":0}')
    ws.recv()

    hit_count = 0

    for row in range(10):
        for column in range(10):
            data = {"Type":"FireForEffect","Cell":[column, row]}
            ws.send(json.dumps(data))
            ready = False

            while not ready:
                response = ws.recv()
                response = json.loads(response)
                if response["Type"] == "Redirect":
                    print(f"({row}, {column})")
                    hit_count += 1
                    print(f"[+] Done, {hit_count} walls found!")
                    sys.exit(0)
                elif response["Type"] == "SplashOver":
                    ready = True
                    if response["Message"] == "HIT!":
                        print(f"({row}, {column})")
                        hit_count += 1
                
            ws.send('{"Type":"SplashOut"}')

    print("[+] Could not complete game...")

if __name__ == "__main__":
    main()