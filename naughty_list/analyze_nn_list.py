import argparse
import hashlib
import typing

import mt19937predictor

import naughty_nice

class NaughtyNiceAnalyzer:
    def __init__(self, chain_file: str):
        self.chain = naughty_nice.Chain(load=True, filename=chain_file)
        self.predictor = mt19937predictor.MT19937Predictor()
        past_values = 0

        for block in self.chain.blocks:
            if past_values < 625:
                self.predictor.setrandbits(block.nonce, 64)
                past_values += 2
            else:
                assert self.predictor.getrandbits(64) == block.nonce

    def predict_next_nonce(self) -> int:
        return self.predictor.getrandbits(64)

    def find_block(self, hex_hash: str) -> typing.Tuple[int, int]:
        for index, block in enumerate(self.chain.blocks):
            sha256 = hashlib.sha256(block.block_data_signed()).hexdigest()
            if sha256 == hex_hash:
                return index, block.index

        return -1

    def hash_block(self, block_index: int, hash_alg: str) -> str:
        if hash_alg in ["md5", "sha256"]:
            hash_obj = getattr(hashlib, hash_alg)
            return hash_obj(self.chain.blocks[block_index].block_data_signed()).hexdigest()

def predict(chain_file: str, count: int) -> None:
    analyzer = NaughtyNiceAnalyzer(chain_file)

    last_id = analyzer.chain.blocks[-1].index

    last_nonce = analyzer.chain.blocks[-1].nonce
    print("{}: {} (0x{:08x}, last block of chain)".format(last_id, last_nonce, last_nonce))

    for i in range(1, count):
        next_nonce = analyzer.predict_next_nonce()
        print("{}: {} (0x{:08x})".format(last_id + i, next_nonce, next_nonce))

def find_block(chain_file: str, hex_hash: str) -> None:
    analyzer = NaughtyNiceAnalyzer(chain_file)

    block_index, block_id = analyzer.find_block(hex_hash)

    if block_index != -1:
        print(f"Block {block_id} found at index {block_index}")
    else:
        print("No block found with that SHA256 hash")

def hash_block(chain_file: str, block_index: int, hash_alg: str) -> None:
    analyzer = NaughtyNiceAnalyzer(chain_file)
    block_hash = analyzer.hash_block(block_index, hash_alg)
    print("Hash of block at index {}: {}".format(block_index, block_hash))


def main():
    parser = argparse.ArgumentParser(description="Analyze Santa's Naughty-Nice Blockchain")
    subparsers = parser.add_subparsers(dest="command", help="Analysis to run")
    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument("dat_file", help="filename of blockchain file")

    nonce_parser = subparsers.add_parser(
        "predict",
        parents=[parent_parser],
        help="Predict the next nonces that will be used"
    )
    nonce_parser.add_argument("-c", "--count", type=int, help="Number of nonces to predict", default=10)

    find_parser = subparsers.add_parser(
        "find",
        parents=[parent_parser],
        help="Find the block with the given SHA256"
    )
    find_parser.add_argument("hash", help="SHA256 of block to find")

    hash_parser = subparsers.add_parser(
        "hash",
        parents=[parent_parser],
        help="Hash a block"
    )
    hash_parser.add_argument("index", type=int, help="Index of block to hash")
    hash_parser.add_argument("-a", "--algorithm", choices=["md5", "sha256"], help="Hash algorithm to use", default="sha256")

    args = parser.parse_args()

    if args.command == "predict":
        predict(args.dat_file, args.count)
    elif args.command == "find":
        find_block(args.dat_file, args.hash)
    elif args.command == "hash":
        hash_block(args.dat_file, args.index, args.algorithm)

if __name__ == "__main__":
    main()
