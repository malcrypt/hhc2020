# :fontawesome-solid-egg: Easter Eggs/Fun Facts
The Counter Hack team always adds fun Easter Eggs throughout the game.

* If Snowball Fight is played in a separate window rather than in the `iframe` of HHC, the PlayerID is `HughRansomDrysdale`, the villian in [Knives Out](https://en.wikipedia.org/wiki/Knives_Out) played by Chris Evans
* The default name in the kiosk tag printer is "Santa's Little Helper" - the family dog from [The Simpsons](https://en.wikipedia.org/wiki/The_Simpsons)
* The picture of Santa's desk includes items from past Holiday Hack Challenges:
    * The Elf legs on the left are from HHC 2015
    * The Tardis was an Easter Egg from HHC 2016
    * The German Enigma on the right has an amusing backstory: [Please Keep Your Brain Juice Off My Enigma](https://www.youtube.com/watch?v=HkSs_wc_bTs)
* The portrait in the Entryway is of course of Ed Skoudis, and he [confirmed](https://twitter.com/edskoudis/status/1338981827969748993) that it was inspired by the portrait from Knives Out
* The suit of armor in Santa's office appears to be an homage to Ed's [steampunk office](http://thesteampunkhome.blogspot.com/2011/02/eds-office-tour.html)
* Santa's office has a picture of all the players congregated in the Courtyard of Elf University during KringleCon 2
* The meeting minutes retrieved in [ARP Shenanigans](objectives/obj9.md) contain several Easter Eggs
    * The names are from the Rankin-Bass Christmas specials or characters from the HHC:
        * Jack Frost - [Jack Frost](https://en.wikipedia.org/wiki/Jack_Frost_(TV_special))
        * Mother Nature, Snow Miser, Heat Miser, Kris Kringle - [The Year Without a Santa Claus](https://en.wikipedia.org/wiki/The_Year_Without_a_Santa_Claus)
        * Clarice, Yukon Cornelius, King Moonracer, Mrs. Donner, Charlie In-the-Box, Dolly [Rudolph the Red-Nosed Reindeer](https://en.wikipedia.org/wiki/Rudolph_the_Red-Nosed_Reindeer_(TV_special))
        * Ginger Breaddie, Krampus, Alabaster Snowball, Queen of the Winter Spirits, Pepper Minstix - HHC
        * Tanta Kringle, Kris Kringle - [Santa Claus Is Comin' to Town](https://en.wikipedia.org/wiki/Santa_Claus_Is_Comin%27_to_Town_(film))
        * Father Time - [Rudolph's Shiny New Year](https://en.wikipedia.org/wiki/Rudolph%27s_Shiny_New_Year)
    * The new business for the meeting hints at a possible plot point for next year's HHC:
> Father Time Castle, new oversized furnace to be installed by Heat Miser Furnace, Inc.  Mr. H. Miser described the plan for installing new furnace to replace the faltering one in Mr. Time’s 20,000 sq ft castle. Ms. G. Breaddie pointed out that the proposed new furnace is 900,000,000 BTUs, a figure she considers “incredibly high for a building that size, likely two orders of magnitude too high.  Why, it might burn the whole North Pole down!”  Mr. H. Miser replied with a laugh, “That’s the whole point!”  The board voted unanimously to reject the initial proposal, recommending that Mr. Miser devise a more realistic and safe plan for Mr. Time’s castle heating system.
    * The meeting started at `7:30 PM North Pole Standard Time,` but [timezones are meaningless at the North Pole](https://blogs.scientificamerican.com/observations/time-has-no-meaning-at-the-north-pole/) due to the fact that 24 times zones meet at a single point.
* Jason is hiding around the Castle just like past HHCs
    * The bucket on the scaffolding to the left of the Castle entrance from the Front Lawn says:
    > Job Hunting? Feeling Stuck? Join me for an interactive job hunting livestream! Any industry. Any type of job. No vendor pitches. Not selling anything. Just helping. https://www.twitch.tv/banjocrashland
    
* The left wall of the Wrapping Room contains an email from Iceman to Chris Elgee discussing if there were any trademark issues with using the Proxmark in the HHC.

![](images/easter_eggs/proxmark-email.png)

* The vending machine in the Speaker UNpreparedness room is called "The Snacken"
* There is a bucket and a step ladder in the kitchen in front of the dishwasher, but they are hidden by CSS. Revealing them doesn't appear to do much, unfortunately...
* When a player leaves the room, the message type is "AUF_WIEDERSEHEN," German for GOODBYE
* When there is a websocket error, the message type is "DENNIS_NEDRY," the computer programmer from [Jurassic Park](https://en.wikipedia.org/wiki/Jurassic_Park_(film))

There are likely many more!
