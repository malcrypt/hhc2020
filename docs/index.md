#:fontawesome-solid-candy-cane: Welcome to KringleCon 3: French Hens (2020)!
![](images/intro.png)

Santa has welcomed the security community back to the North Pole for KringleCon 3: French Hens. This year the event is being held at Santa's newly renovated North Pole castle. If you haven't had a chance, be sure to check out [Ed Skoudis' talk](https://youtu.be/8e0SZrbWFuU) about how to get started with the Holiday Hack Challenge.

This walkthrough is broken down into a several sections:

* [Map](map.md) - Map of the castle to help you find your way around
* [Items](items.md) - There are items strewn about the castle that will help you complete challenges. This lists all known items and their importance in the game
* [Narrative](narrative.md) - This is the story that unfolds as you complete the Main Objectives
* [Main Objectives](objectives/obj1.md) - The 12 objectives that must be completed to finish the story
* [Side Missions](side_missions/tmux.md) - The optional mini-games and terminal challenges that will give you hints on how to complete the Main Objectives
* [Easter Eggs](easter_eggs.md) - The Counter Hack team often hides fun easter eggs throughout the game. This page documents know Easter Eggs and points out fun facts about topics and items found throughout the game


When you're ready, let's take the New Jersey Turnpike to get to the gondola that will transport us to the North Pole.

[![New Jersey Turnpike](images/turnpike.png)](map.md)
