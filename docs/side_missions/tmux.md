#![](../images/cranpi.png) Unescape Tmux

**TL;DR - [Answer](#answer)**

## Location
Top-left corner of the Front Lawn ([see map](../map.md#ground-floor))

## Hints

> There's a handy tmux reference available at [https://tmuxcheatsheet.com/](https://tmuxcheatsheet.com/)!

## Solution

When we connect to the terminal we are met with the MOTD:
```code
Can you help me?

I was playing with my birdie (she's a Green Cheek!) in something called tmux,
then I did something and it disappeared!

Can you help me find her? We were so attached!!
```

We can check the current `tmux` sessions using the `ls` command
```console
elf@2e444f27fe4b:~$ tmux ls
0: 1 windows (created Wed Dec 16 17:31:38 2020) [80x24]
```

The list shows a single, unnamed session `0`. Let's attach to that session
```console
elf@2e444f27fe4b:~$ tmux attach -t 0
```

Aha! There's the birdie.

Practice moving around `tmux` as it will be very helpful when you get to [Objective 9](../objectives/obj9.md)

## Answer
Execute **`tmux attach -t 0`**
