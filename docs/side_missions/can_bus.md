#![](../images/cranpi.png) CAN-Bus Investigation

Wunorse Openslae:
> Hiya hiya - I'm Wunorse Openslae!

> I've been playing a bit with CAN bus. Are you a car hacker?

> I'd love it if you could take a look at this terminal for me.

> I'm trying to figure out what the unlock code is in this CAN bus log.

> When it was grabbing this traffic, I locked, unlocked, and locked the doors one more time.

> It ought to be a simple matter of just filtering out the noise until we get down to those three actions.

> Need more of a nudge? Check out Chris Elgee's [talk](https://www.youtube.com/watch?v=96u-uHRBI0I) on CAN traffic!

**TL;DR - [Answer](#answer)**

## Location
On the left side of the roof near the sleigh ([see map](../map.md#roofnetwars))

## Hints

> You can hide lines you don't want to see with commands like `cat file.txt | grep -v badstuff`

> Chris Elgee is talking about how [CAN traffic](https://www.youtube.com/watch?v=96u-uHRBI0I) works right now!

## Solution
The easiest way to accomplish this challenge is to wait until you are Santa (after completing [Objective 5](../objectives/obj5.md)). As Santa, you can access the sleigh on the roof. To start, let's set up filters to hide all of the recurring messages

| ID  | Operator | Criterion    |
| --- | -------- | ------------ |
| 018 | Equals   | 000000000000 |
| 244 | Equals   | 000000000000 |
| 188 | Equals   | 000000000000 |
| 019 | Equals   | 000000000000 |
| 080 | Equals   | 000000000000 |
| 19B | Equals   | 0000000F2057 |

At this point there shouldn't be any new messages scrolling across the screen. Now we can press the ++"Lock"++ and ++"Unlock"++ buttons and see only the messages we care about. Pressing ++"Lock"++ produces the message `19B#000000000000`, and ++"Unlock"++ produces the message `19B#00000F000000`. With this knowledge we can go to the CAN-Bus terminal and grep for just the `Unlock` message.

```console
elf@9a18d3ed4716:~$ ls
candump.log  runtoanswer
elf@9a18d3ed4716:~$ grep "19B#00000F000000" candump.log 
(1608926671.122520) vcan0 19B#00000F000000
elf@9a18d3ed4716:~$ ./runtoanswer 122520
Your answer: 122520

Checking....
Your answer is correct!
```
Submit the decimal portion of the timestamp (`122520`) to `runanswer` to complete the challenge.

## Answer
```shell
./runtoanswer 122520
```
