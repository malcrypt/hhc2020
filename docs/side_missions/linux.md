#![](../images/cranpi.png) Linux Primer

Sugarplum Mary:
> Sugarplum Mary? That's me!

> I was just playing with this here terminal and learning some Linux!

> It's a great intro to the Bash terminal.

> If you get stuck at any point, type `hintme` to get a nudge!

> Can you make it to the end?

**TL;DR**
```shell
yes
ls ~
cat munchkin_19315479765589239
rm munchkin_19315479765589239
pwd
ls -la
grep "munchkin" .bash_history
env
cd workshop
grep -i -r "munchkin" . 
chmod +x lollipop_engine
./lollipop_engine
cd electrical
mv blown_fuse0 fuse0
ln -s fuse0 fuse1
cp fuse1 fuse2
echo "MUNCHKIN_REPELLENT" >> fuse2
find /opt/munchkin_den -iname "*munchkin*"
find /opt/munchkin_den/ -user munchkin
find /opt/munchkin_den/ -size +108k -size -110k
ps aux| grep -i "munchkin"
netstat -tl
curl 127.0.0.1:54321/
kill PID
exit
```

## Location
Middle of the Courtyard ([see map](../map.md#ground-floor))

## Solution
> All the lollipops on this system have been stolen by munchkins. Capture munchkins by following instructions here and 🍭's will appear in the green bar below. Run the command "hintme" to receive a hint. Type "yes" to begin:

```shell
yes
```

> Perform a directory listing of your home directory to find a munchkin and retrieve a lollipop!

* The `ls` command allows you to list the contents of a directory, and `~` is the Linux shortcut for your home directory.

```shell
ls ~
```

> Now find the munchkin inside the munchkin.

* There's a file in your home directory called `munchkin_1931547976558923`. Let's view it's contents

```shell
cat munchkin_1931547976558923
```

> Great, now remove the munchkin in your home directory.

* Use the `rm` command to remove the previous file

```shell
rm munchkin_1931547976558923
```

> Print the present working directory using a command.

* `pwd` prints the present working directory in Linux

```shell
pwd
```

> Good job but it looks like another munchkin hid itself in you home directory. Find the hidden munchkin!

* By default `ls` does not show hidden files (those with a leading `.`). Use the `-a` flag to show everything

```shell
ls -a ~
```

> Excellent, now find the munchkin in your command history.

* The `grep` command allows us to search for strings and regex patterns in files
* When using `bash`, your command history is stored in `~/.bash_history`. Let's search this file for the munchkin

```shell
grep "munchkin" ~/.bash_history
```

> Find the munchkin in your environment variables.

* The environment variables of the current shell can be printed with the `env` command

```shell
env
```

> Next, head into the workshop.

* The `cd` command allows us to change directories. Paths can be relative to the present working directory (`workshop`) or absolute (`home/elf/workshop`)

```shell
cd workshop
```

> A munchkin is hiding in one of the workshop toolboxes. Use `grep` while ignoring case to find which toolbox the munchkin is in.

* With `grep` we can ignore case by using the `-i` flag, and can search a directory recursively with the `-r` flag. `.` in this context is a Linux shortcut meaning the current directory.

```shell
grep -i -r "munchkin" .
```

> A munchkin is blocking the lollipop_engine from starting. Run the lollipop_engine binary to retrieve this munchkin.

* Linux will not allow an executable file to run unless the execution permission is set. If we run the command `ls -la lollipop_engine` we see that the binary does not have the execution permission. We can add execute permission with `chmod`, then can run the binary

```shell
chmod +x lollipop_engine
./lollipop_engine
```

> Munchkins have blown the fuses in /home/elf/workshop/electrical. cd into electrical and rename blown_fuse0 to fuse0.

* Files are renamed using the `mv` command

```shell
cd electrical
mv blown_fuse0 fuse0
```

> Now, make a symbolic link (symlink) named fuse1 that points to fuse0

* A symbolic link, or symlink for short, is when a file pointers to [another file on disk](https://en.wikipedia.org/wiki/Symbolic_link). In Linux they are created with the `ln` command and the `-s` flag.

```shell
ln -s fuse0 fuse1
```

> Make a copy of fuse1 named fuse2.

* Files are copied using the `cp` command

```shell
cp fuse1 fuse2

```

> We need to make sure munchkins don't come back. Add the characters "MUNCHKIN_REPELLENT" into the file fuse2.

* `echo` allows us to print information to the screen. We can use the redirection operators to send this echoed output to a file, either overwritting the original contents (`>`) or appending to it (`>>`)

```shell
echo "MUNCHKIN_REPELLENT" >> fuse2
```

> Find the munchkin somewhere in /opt/munchkin_den.

* The `find` command allows us to search for files in the filesystem based on properties such as name, size, owner, permissions, etc. The `-iname` argument allows us to search by filename and ignore case

```shell
find /opt/munchkin_den/ -iname "*munchkin*"
```

> Find the file somewhere in /opt/munchkin_den that is owned by the user munchkin.

* `-user` flag allows us to search by the file's owning user

```shell
find /opt/munchkin_den/ -user munchkin
```

> Find the file created by munchkins that is greater than 108 kilobytes and less than 110 kilobytes located somewhere in /opt/munchkin_den.

* `-size` can be used to look for a file with an exact size, or a size greater than (preceding `+`), or less than (preceding `-`) a given value. The flag can be repeated multiple times to set a size range

```shell
find /opt/munchkin_den/ -size +108k -size -110k
```

> List running processes to find another munchkin.

* Processes can be listed with the `ps` command with the following flags
    * `a` show processes for all users
    * `u` display the process's owner
    * `x` show processes not attached to a terminal

```shell
ps aux| grep -i "munchkin"
```

> The 14516_munchkin process is listening on a tcp port. Use a command to have the only listening port display to the screen.

* The `netstat` (`ss` on modern systems) command prints network-related information. We can then use flag `-l` to only show listening ports, and `-t` to only show TCP ports

```shell
netstat -tl
```

> The service listening on port 54321 is an HTTP server. Interact with this server to retrieve the last munchkin.

* The `curl` command can be used to interact with an HTTP(S) server.

```shell
curl 127.0.0.1:54321
```

> Your final task is to stop the 14516_munchkin process to collect the remaining lollipops.

* Processes can be killed with the `kill` command followed by the process ID (pid), which we saw in the second column of the `ps aux` output. Your pid will likely be different.

```shell
kill 34549
```

> Congratulations, you caught all the munchkins and retrieved all the lollipops!

> Type "exit" to close...

```shell
exit
```



