#:fontawesome-solid-gamepad: 33.6 kbps

Fitzy Shortstack
> "Put it in the cloud," they said...

> "It'll be great," they said...

> All the lights on the Christmas trees throughout the castle are controlled through a remote server.

> We can shuffle the colors of the lights by connecting via dial-up, but our only modem is broken!"

> Fortunately, I speak dial-up. However, I can't quite remember the [handshake sequence](https://upload.wikimedia.org/wikipedia/commons/3/33/Dial_up_modem_noises.ogg).

> Maybe you can help me out? The phone number is **756-8347**; you can use this blue phone.

## Location
Upper-right corner of the Kitchen ([see map](../map.md#ground-floor))

## Solution
For anyone old enough to remember dial-up modems, the sound of the dial-up handshake sequence will likely send a chill down your spine. For you young whipper-snappers that don't, consider yourselves lucky :fontawesome-solid-smile:.

There's not much technical skills necessary to get through this challenge. Listen to the handshake sequence, and try to emulate it by clicking the words on the piece of paper in time with the handshake sounds on the other end of the line.

The correct steps:

1. Click the handset to get a dial tone
1. Dial **756-8347**
1. At the appropiate times (based on what you heard in the example handshake) press
    * `baa DEE brrr`
    * `aah`
    * `WEWEWWrwrrwrr`
    * `beDURRdunditty`
    * `SCHHRRHHRTHRTR`

A voice on the phone will say that the lights have been updated, and the lights on the Christmas tree will change.


