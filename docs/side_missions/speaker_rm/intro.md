# ![](../../images/cranpi.png) Speaker UNpreparedness Room Terminal
The terminal outside of the Speaker UNpreparedness room contains three different challenges:

* [Door](door.md) - Unlock the door to the room
* [Lights](lights.md) - Turn on the lights in the room
* [Vending Machine](vending.md) - Turn on the vending machine in the room

## Location
Just outside the Speaker UNpreparedness room in the Talks Lobby ([see map](../../map.md#floor-2))
