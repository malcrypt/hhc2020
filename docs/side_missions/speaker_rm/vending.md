#![](../../images/cranpi.png) Speaker UNpreparedness Room - Vending Machine

Bushy Evergreen:
> So hey, if you want, there's one more challenge.

> You see, there's a vending machine in there that the speakers like to use sometimes.

> Play around with `./vending_machines` in the lab folder.

> You know what might be worth trying? Delete or rename the config file and run it.

> Then you could set the password yourself to AAAAAAAA or BBBBBBBB.

> If the encryption is simple code book or rotation ciphers, you'll be able to roll back the original password.

**TL;DR - [Answer](#answer)**

## Location
Just outside the Speaker UNpreparedness room in the Talks Lobby ([see map](../../map.md#floor-2))

## Hints

> For polyalphabetic ciphers, if you have control over inputs and visibilty of outputs, lookup tables can save the day.

## Solution
Bushy hasn't lead us wrong yet, so let's try what he suggests. If we remove `vending-machine.json`, the program asks us for a new name and password. Hmm, this sounds like a [chosen plaintext attack](https://en.wikipedia.org/wiki/Chosen-plaintext_attack). Let's try giving it a long string and see if any information about the plaintext leaks into the ciphertext.
```console
elf@7bb95e7e9551 ~ $ cd lab/
elf@7bb95e7e9551 ~/lab $ rm vending-machines.json 
elf@7bb95e7e9551 ~/lab $ ./vending-machines 
The elves are hungry!

If the door's still closed or the lights are still off, you know because
you can hear them complaining about the turned-off vending machines!
You can probably make some friends if you can get them back on...

Loading configuration from: /home/elf/lab/vending-machines.json

I wonder what would happen if it couldn't find its config file? Maybe that's
something you could figure out in the lab...

ALERT! ALERT! Configuration file is missing! New Configuration File Creator Activated!

Please enter the name > elf
Please enter the password > aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa

Welcome, elf! It looks like you want to turn the vending machines back on?
Please enter the vending-machine-back-on code > ^C
elf@7bb95e7e9551 ~/lab $ cat vending-machines.json 
{
  "name": "elf",
  "password": "9Vbtacpg9Vbtacpg9Vbtacpg9Vbtacpg9Vbtacpg9Vbtacpg9"
}
```

Interesting. With a plaintext composed of the same character repeated numerous times, the ciphertext produces the same 8 characters over and over. This likely means that the block length of the cipher is 8 characters. Let's check that each ciphertext character is independent of one another.
```console
elf@cb7924e5841d ~/lab $ rm vending-machines.json 
elf@cb7924e5841d ~/lab $ ./vending-machines 
The elves are hungry!

If the door's still closed or the lights are still off, you know because
you can hear them complaining about the turned-off vending machines!
You can probably make some friends if you can get them back on...

Loading configuration from: /home/elf/lab/vending-machines.json

I wonder what would happen if it couldn't find its config file? Maybe that's
something you could figure out in the lab...

ALERT! ALERT! Configuration file is missing! New Configuration File Creator Activated!

Please enter the name > elf
Please enter the password > abcdefgh            

Welcome, elf! It looks like you want to turn the vending machines back on?
Please enter the vending-machine-back-on code > ^C
elf@cb7924e5841d ~/lab $ cat vending-machines.json 
{
  "name": "elf",
  "password": "9UedAffh"
}
```

In this case we see that if `a` is in position `0`, it will encrypt to `9` no matter the other characters in the block. With this information we are likely dealing with a [polyalphabetic cipher](https://en.wikipedia.org/wiki/Polyalphabetic_cipher) with the block size of 8. If we build 8 lookup tables mapping all possible plaintext characters to their cooresponding ciphertext characters at that index, we can decrypt the original ciphertext.

These lookup tables can be constructed by entering a password into the program that contains multiple blocks where each block contains a single character from the plaintext alphabet (believed to be `[a-zA-Z0-9]`). The first block will allow us to see the resulting ciphertext of `a` at all 8 possible indexes. The subsequent blocks will discover the ciphertext for the remaining plaintext alphabet. While this could be done by hand, [vending_pwd.py](https://gitlab.com/malcrypt/hhc2020/-/blob/master/vending_machine/vending_pwd.py) will handle the heavy lifting for us.

First we will generate the password to use in our chosen plaintext attack:
```console
$ python3 vending_pwd.py gen 8
aaaaaaaabbbbbbbbccccccccddddddddeeeeeeeeffffffffgggggggghhhhhhhhiiiiiiiijjjjjjjjkkkkkkkkllllllllmmmmmmmmnnnnnnnnooooooooppppppppqqqqqqqqrrrrrrrrssssssssttttttttuuuuuuuuvvvvvvvvwwwwwwwwxxxxxxxxyyyyyyyyzzzzzzzzAAAAAAAABBBBBBBBCCCCCCCCDDDDDDDDEEEEEEEEFFFFFFFFGGGGGGGGHHHHHHHHIIIIIIIIJJJJJJJJKKKKKKKKLLLLLLLLMMMMMMMMNNNNNNNNOOOOOOOOPPPPPPPPQQQQQQQQRRRRRRRRSSSSSSSSTTTTTTTTUUUUUUUUVVVVVVVVWWWWWWWWXXXXXXXXYYYYYYYYZZZZZZZZ00000000111111112222222233333333444444445555555566666666777777778888888899999999

```

We enter the resulting string as the password to `vending-machines` and get the resulting ciphertext:
```console
elf@2f611ae0072a ~/lab $ rm vending-machines.json 
elf@2f611ae0072a ~/lab $ ./vending-machines 
The elves are hungry!

If the door's still closed or the lights are still off, you know because
you can hear them complaining about the turned-off vending machines!
You can probably make some friends if you can get them back on...

Loading configuration from: /home/elf/lab/vending-machines.json

I wonder what would happen if it couldn't find its config file? Maybe that's
something you could figure out in the lab...

ALERT! ALERT! Configuration file is missing! New Configuration File Creator Activated!

Please enter the name > elf
Please enter the password > aaaaaaaabbbbbbbbccccccccddddddddeeeeeeeeffffffffgggggggghhhhhhhhiiiiiiiijjjjjjjjkkkkkkkkllllllllmmmmmmmmnnnnnnnnooooooooppppppppqqqqqqqqrrrrrrrrssssssssttttttttuuuuuuuuvvvvvvvvwwwwwwwwxxxxxxxxyyyyyyyyzzzzzzzzAAAAAAAABBBBBBBBCCCCCCCCDDDDDDDDEEEEEEEEFFFFFFFFGGGGGGGGHHHHHHHHIIIIIIIIJJJJJJJJKKKKKKKKLLLLLLLLMMMMMMMMNNNNNNNNOOOOOOOOPPPPPPPPQQQQQQQQRRRRRRRRSSSSSSSSTTTTTTTTUUUUUUUUVVVVVVVVWWWWWWWWXXXXXXXXYYYYYYYYZZZZZZZZ00000000111111112222222233333333444444445555555566666666777777778888888899999999

Welcome, elf! It looks like you want to turn the vending machines back on?
Please enter the vending-machine-back-on code > ^C
elf@2f611ae0072a ~/lab $ cat vending-machines.json 
{
  "name": "elf",
  "password": "9VbtacpgGUVBfWhPe9ee6EERORLdlwWbwcZQAYue8wIUrf5xkyYSPafTnnUgokAhM0sw4eOCa8okTqy1o63i07r9fm6W7siFqMvusRQJbhE62XDBRjf2h24c1zM5H8XLYfX8vxPy5NAyqmsuA5PnWSbDcZRCdgTNCujcw9NmuGWzmnRAT7OlJK2X7D7acF1EiL5JQAMUUarKCTZaXiGRehmwDqTpKv7fLbn3UP9Wyv09iu8Qhxkr3zCnHYNNLCeOSFJGRBvYPBubpHYVzka18jGrEA24nILqF14D1GnMQKdxFbK363iZBrdjZE8IMJ3ZxlQsZ4Uisdwjup68mSyVX10sI2SHIMBo4gC7VyoGNp9Tg0akvHBEkVH5t4cXy3VpBslfGtSz0PHMxOl0rQKqjDq2KtqoNicv3ehm9ZFH2rDO5LkIpWFLz5zSWJ1YbNtlgophDlgKdTzAYdIdjOx0OoJ6JItvtUjtVXmFSQw4lCgPE6x7"
```

We then feed the ciphertext from the attack and the original ciphertext from the JSON file to the script to get the original password:
```console hl_lines="3"
$ python3 vending_pwd.py dec 8 "LVEdQPpBwr" "9VbtacpgGUVBfWhPe9ee6EERORLdlwWbwcZQAYue8wIUrf5xkyYSPafTnnUgokAhM0sw4eOCa8okTqy1o63i07r9fm6W7siFqMvusRQJbhE62XDBRjf2h24c1zM5H8XLYfX8vxPy5NAyqmsuA5PnWSbDcZRCdgTNCujcw9NmuGWzmnRAT7OlJK2X7D7acF1EiL5JQAMUUarKCTZaXiGRehmwDqTpKv7fLbn3UP9Wyv09iu8Qhxkr3zCnHYNNLCeOSFJGRBvYPBubpHYVzka18jGrEA24nILqF14D1GnMQKdxFbK363iZBrdjZE8IMJ3ZxlQsZ4Uisdwjup68mSyVX10sI2SHIMBo4gC7VyoGNp9Tg0akvHBEkVH5t4cXy3VpBslfGtSz0PHMxOl0rQKqjDq2KtqoNicv3ehm9ZFH2rDO5LkIpWFLz5zSWJ1YbNtlgophDlgKdTzAYdIdjOx0OoJ6JItvtUjtVXmFSQw4lCgPE6x7"
The password is:
CandyCane1
```

## Answer
**CandyCane1**
