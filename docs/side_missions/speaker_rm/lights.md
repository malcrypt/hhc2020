#![](../../images/cranpi.png) Speaker UNpreparedness Room - Lights

Bushy Evergreen:
> Hey, you want to help me figure out the light switch too? Those come in handy sometimes.

> The password we need is in the `lights.conf` file, but it seems to be encrypted.

> There's another instance of the program and configuration in `~/lab/` you can play around with.

> What if we set the user name to an encrypted value?

**TL;DR - [Answer](#answer)**

## Location
Just outside the Speaker UNpreparedness room in the Talks Lobby ([see map](../../map.md#floor-2))

## Hints

> While you have to use the `lights` program in `/home/elf/` to turn the lights on, you can delete parts in `/home/elf/lab/`.

## Solution
Well, Bushy pretty much gives us this one. Apparently if the `name` in the config file is encrypted, the program will decrypt it before printing it to the screen. Convenient, huh?

In the `lab` directory we can modify `lights.conf` so that the `name` is the encrypted password. When the program is run, it will decrypt `name` and print the plaintext to the screen.

```console hl_lines="15"
elf@1a1fd70aa3df ~ $ cd lab
elf@1a1fd70aa3df ~/lab $ cat lights.conf 
password: E$ed633d885dcb9b2f3f0118361de4d57752712c27c5316a95d9e5e5b124
name: E$ed633d885dcb9b2f3f0118361de4d57752712c27c5316a95d9e5e5b124
elf@1a1fd70aa3df ~/lab $ ./lights
The speaker unpreparedness room sure is dark, you're thinking (assuming
you've opened the door; otherwise, you wonder how dark it actually is)

You wonder how to turn the lights on? If only you had some kind of hin---

 >>> CONFIGURATION FILE LOADED, SELECT FIELDS DECRYPTED: /home/elf/lab/lights.conf

---t to help figure out the password... I guess you'll just have to make do!

The terminal just blinks: Welcome back, Computer-TurnLightsOn

What do you enter? >
```

With the password decrypted, we can run the real `lights` binary to turn on the lights

```console
elf@1a1fd70aa3df ~/lab $ cd ..
elf@1a1fd70aa3df ~ $ ./lights 
The speaker unpreparedness room sure is dark, you're thinking (assuming
you've opened the door; otherwise, you wonder how dark it actually is)

You wonder how to turn the lights on? If only you had some kind of hin---

 >>> CONFIGURATION FILE LOADED, SELECT FIELDS DECRYPTED: /home/elf/lights.conf

---t to help figure out the password... I guess you'll just have to make do!

The terminal just blinks: Welcome back, elf-technician

What do you enter? > Computer-TurnLightsOn
Checking......

Lights on!
```

## Answer
**Computer-TurnLightsOn**
