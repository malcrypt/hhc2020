# ![](../../images/cranpi.png) Speaker UNpreparedness Room - Door

Bushy Evergreen:
> Ohai! Bushy Evergreen, just trying to get this door open.

> It's running some Rust code written by Alabaster Snowball.

> I'm pretty sure the password I need for `./door` is right in the executable itself.	

> Isn't there a way to view the human-readable `strings` in a binary file?

**TL;DR - [Answer](#answer)**

## Location
Just outside the Speaker UNpreparedness room in the Talks Lobby ([see map](../../map.md#floor-2))

## Hints

> The `strings` command is common in Linux and available in Windows as part of SysInternals.

## Solution
The `door` binary contains the password in plaintext. Completing this challenge is as simple as:
```console hl_lines="5"
elf@e31cd7d29570 ~ $ strings door |grep "password"
/home/elf/doorYou look at the screen. It wants a password. You roll your eyes - the 
password is probably stored right in the binary. There's gotta be a
Be sure to finish the challenge in prod: 
And don't forget, the password is "Op3nTheD00r"
```

## Answer
**Op3nTheD00r**
