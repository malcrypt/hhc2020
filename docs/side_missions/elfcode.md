#:fontawesome-solid-gamepad: Elf Code

Ribb Bonbowford:
> This challenge centers around JavaScript. Take a look at this intro and see how far it gets you!"

> Ready to move beyond `elf` commands? Don't be afraid to mix in native JavaScript."

> Trying to extract only numbers from an array? Have you tried to `filter`?

> Maybe you need to enumerate an object's keys and then filter?

> Getting hung up on number of lines? Maybe try to minify your code.

> Is there a way to `push` array items to the beginning of an array? Hmm...

## Location
Upper-left corner of the Dining Room ([see map](../map.md#ground-floor))

## Hints

> Did you try the JavaScript primer? There's a great section on looping.

> Want to learn a useful language? [JavaScript](https://jgthms.com/javascript-in-14-minutes/) is a great place to start! You can also test out your code using a [JavaScript playground](https://playcode.io/).

> There are lots of ways to [make your code shorter](https://jscompress.com/), but the number of elf commands is key.

> [There's got to be a way to filter for specific typeof items in an array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray/filter). Maybe [the typeof operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof) could also be useful?

> `var array = [2, 3, 4]; array.push(1)` doesn't do QUITE what was intended...

> [In JavaScript you can enumerate an object's keys using keys, and filter the array using filter.](https://stackoverflow.com/questions/9907419/how-to-get-a-key-in-a-javascript-object-by-its-value)

## Solution - Level 1
![Level 1](../images/elfcode/elfcode_l1.png)

### Restrictions
* No more than 2 lines of code
* No more than 2 elf commands

### Answer
The lollipop is 10 spaces left from the start, and the exit is 10 spaces up from the lollipop.

```javascript linenums="1"
elf.moveLeft(10)
elf.moveUp(10)
```

## Solution - Level 2
![Level 2](../images/elfcode/elfcode_l2.png)

### Restrictions
* No more than 5 lines of code
* No more than 5 elf commands

### Challenges
* Lever 0 - Add 2 to the number that the lever returns

### Answer
`moveTo` the first lever, answer it's challenge, then move to the exit.
```javascript linenums="1"
elf.moveTo(lever[0])
elf.pull_lever(elf.get_lever(0) + 2)
elf.moveLeft(4)
elf.moveUp(10)
```

## Solution - Level 3
![Level 3](../images/elfcode/elfcode_l3.png)

### Restrictions
* No more than 4 lines of code
* No more than 4 elf commands

### Answer
The `moveTo` function makes this level very easy.
```javascript linenums="1"
elf.moveTo(lollipop[0])
elf.moveTo(lollipop[1])
elf.moveTo(lollipop[2])
elf.moveUp(1)
```

## Solution - Level 4
![Level 4](../images/elfcode/elfcode_l4.png)

### Restrictions
* No more than 7 lines of code
* No more than 6 elf commands

### Answer
For this level, it helps to remember that the elf will stop when it reaches a barrier, even if there are more steps to be taken. Thus, getting through this level is a matter of moving left, up, left, down 3 times.

```javascript linenums="1"
for (var i = 0; i < 3; i++) {
  elf.moveLeft(3)
  elf.moveUp(40)
  elf.moveLeft(3)
  elf.moveDown(40)
}
```

## Solution - Level 5
![Level 5](../images/elfcode/elfcode_l5.png)

### Restrictions
* No more than 10 lines of code
* No more than 5 elf commands

### Challenges
* Muchkin 0 - Return the given array, removing the strings and leaving only the integers

### Answer
`moveTo` the munchkin gets the lollipops automatically. To pass the munchkin's challenge, `filter` can be used with an [arrow function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions) to return only the list items that are integers.

```javascript linenums="1"
elf.moveTo(munchkin[0])
var given = elf.ask_munch(0)
var answer = given.filter(item => Number.isInteger(item))
elf.tell_munch(answer)
elf.moveUp(2)
```

## Solution - Level 6
![Level 6](../images/elfcode/elfcode_l6.png)

### Restrictions
* No more than 15 lines of code
* No more than 7 elf commands

### Challenges
* Muchkin 0 - Given a JSON object, return the key that contains the value `lollipop`

### Answer
`moveTo` allows us to get to lollipop 3, but it must be done in a loop to keep the elf command count down. Once at the last lollipop, you can move directly to the munchkin. The munchkin's challenge can be completed by iterating through all of the keys of the JSON object and finding the one with the value of `lollipop`.

```javascript linenums="1"
for(var i=0; i < 4; i++){
  elf.moveTo(lollipop[i])
}
elf.moveTo(munchkin[0])
var given = elf.ask_munch(0)
for (key in given) {
  if (given[key] == "lollipop") {
    elf.tell_munch(key)
    elf.moveUp(2)
  }
}
```

## Solution - Bonus Level 1
![Bonus Level 1](../images/elfcode/elfcode_bl1.png)

### Restrictions
* No more than 25 lines of code
* No more than 10 elf commands

### Challenges
* Lever n - Return the lever number (eg 0 => lever0, 1 => lever1, etc)
* Munchkin 0 - Answer with a function that, when given an array of arrays, will return the sum of all the integers in the child arrays

### Answer
If you look carefully at the number of steps that have to be made in each direction, the number increases by 1 each time. We can use an incremented count to control the number of steps. To keep the elf command count down, we can add the four direction functions (down, left, up, right) to an array and execute the movement command at number of steps mod 4. Finally, the munchkin challenge is similar to Level 5, except rather than just removing all non-integers we're removing them and dealing with a 2D array.

```javascript linenums="1"
function beat_munchkin(arg) {
  var total = 0;
  for (var i = 0; i < arg.length; i++) {
    var innerArray = arg[i].filter(item => Number.isInteger(item))
    for(var j = 0; j < innerArray.length; j++){
        total += innerArray[j]
    }
  }
  return total
}
var steps = 1
var movements = [elf.moveDown, elf.moveLeft, elf.moveUp, elf.moveRight]
for (var i = 0; i < 8; i++) {
  movements[i % 4](steps)
  elf.pull_lever(steps - 1)
  steps += 1
}
elf.moveUp(2)
elf.moveLeft(4)
elf.tell_munch(beat_munchkin)
elf.moveUp(1)

```

## Solution - Bonus Level 2
![Bonus Level 2](../images/elfcode/elfcode_bl2.png)

### Restrictions
* No more than 40 lines of code
* No more than 10 elf commands

### Challenges
* Lever n: Return the sum of all previous lever's outputs plus it's own output
* Munchkin 0: Return a function that takes an array of JSON objects and returns the key that contains the value `lollipop`

### Answer
We keep a running total of the lever's outputs, and add the current lever's output each time we trigger one. The movements are just alternating between right and left, incrementing the number of steps by 2 each time. Finally, the munchkin challenge requires iterating through the array, and performing the same actions from Level 6.

```javascript linenums="1"
function beatMunchkin(arg){
  for(var i = 0; i < arg.length; i++){
    var obj = arg[i]
	for (key in obj) {
	  if (obj[key] == "lollipop") {
		return key
	  }
	}
  }
}
 
var leverSum = 0
var movements = [elf.moveRight, elf.moveLeft]
var steps = 1
for (var i = 0; i < 6; i++) {
  movements[i % 2](steps)
  leverSum += elf.get_lever(i)
  elf.pull_lever(leverSum)
  elf.moveUp(2)
  steps += 2
}
elf.tell_munch(beatMunchkin)
elf.moveRight(40)

```
