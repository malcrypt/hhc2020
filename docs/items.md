# :fontawesome-solid-toolbox: Items
There are various items that can be picked up around the castle. Many of the items are labeled as "Maybe" in the Required column because it depends how you solve the [Santavator challenge](objectives/obj4.md). "Required" assumes you complete the Santavator game "as intended," instead of using JavaScript-fu to bypass it.

|                                      | Item              | Location                                                        | Required |
| ------------------------------------ | ----------------- | --------------------------------------------------------------- | -------- |
|![](images/items/key.png)             | Service Key       | [Talk to Sparkle Redberry](map.md#ground-floor)                 | Yes      |
|![](images/items/greenlight.png)      | Green Light Bulb  | [Courtyard, left side](map.md#ground-floor)                     | Yes      |
|![](images/items/redlight.png)        | Red Light Bulb    | [Talks Lobby, right side](map.md#floor-2)                       | Yes      |
|![](images/items/yellowlight.png)     | Yellow Light Bulb | [Roof, left side](map.md#roofnetwars)                           | Yes      |
|![](images/items/workshop-button.png) | Floor 1.5 Button  | [Speaker UNpreparedness Room](map.md#floor-2)                   | Yes      |
|![](images/items/proxmark.png)        | Proxmark          | [Wrapping Room](map.md#floor-15)                                | Yes      |
|![](images/items/candycane.png)       | Broken Candy Cane | Entrance between [Front Lawn and Entryway](map.md#ground-floor) | Maybe    |
|![](images/items/nut.png)             | Hex Nut 1         | [Entryway, right side](map.md#ground-floor)                     | Maybe    |
|![](images/items/nut2.png)            | Hex Nut 2         | [Dining Room, top of table](map.md#ground-floor)                | Maybe    |
|![](images/items/marble.png)          | Marble 1          | Unknown                                                         | Maybe    |
|![](images/items/marble2.png)         | Marble 2          | [Workshop](map.md#floor-15)                                     | Maybe    |
|![](images/items/ball.png)            | Rubber Ball       | [Wrapping Room](map.md#floor-15)                                | Maybe    |
|![](images/items/portals.png)         | Portals           | [Speaker UNpreparedness Room, Vending Machine](map.md#floor-2)  | Maybe    |
