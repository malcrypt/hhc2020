# :fontawesome-solid-map-marker-alt: KringleCon 3 Map

## :fontawesome-solid-tram: Staging (Bottom of the Mountain)
```{ .code .map }
╭────╮
│Obj1│
╰────╯
            GOND
    ╔═══════╛   ╘═══════╗
    ║             JR    ║
    ║                   ║
    ║                   ║
    ╚═══════════════════╝
           Staging
```

| Label | Description                                                  |
| ----- | ------------------------------------------------------------ |
| GOND  | Gondola to/from Front Lawn                                   |
| Obj1  | [Objective 1: Uncover Santa's Gift List](objectives/obj1.md) |
| JR    | Jingle Ringford                                              |

## :fontawesome-solid-door-open: Ground Floor
```{ .code .map }
                                    Courtyard
╔═══════════════════════════════════════════════════════════════════════════════╗
║         ┌───────────────────────────────────────────────────────────┐         ║
║ GLB     │                          Vendors                          │       ╔═╝
╚═╗       └───────────────────────────────────────────────────────────┘       ║
  ║                                               ┌───┐    ╭────╮             ║
  ║  BL                                           │LPR│ SM │Obj3│             ║
  ║                                    JF         └───┘    ╰────╯             ║
╔═╝                                    XX                                     ╚═╗
║                                                                               ║
╠════════╕   ╒════════╦═══════════════════════════════════╦════════╕   ╒════════╣
║┌───┐ RB             ║         Kitchen           ┌───┐   ║                     ║
║│ELF│                ╙                        FS │336│   ╜                     ║
║└───┘                        ┌───┐               └───┘                         ║
║         HN          ╓       │RED│ HE                    ╖                     ║
║      ┌──────┐       ║       └───┘                       ║                     ║
║      │      │       ╠══════════╦═════════════╤══════════╢                     ║
║      │      │       ║          ║     SA▲     │          ║   ╭────╮            ║
║      │      │       ║          ║             │ Elevator ║   │Obj6│ AC         ║
║      │      │       ╠══════════╝             └──────────╢   ╰────╯            ║
║      │      │       ║        PS               SR        ║                     ║
║      │      │       ║                                 HN║                     ║
║      │      │       ║                              GB   ║                     ║
║      └──────┘       ╜              Entryway             ╙                     ║
║    Dining Room                                                 Great Room     ║
║                     ╖                                   ╓                     ║
╚═════════════════════╬═══════════════╕   ╒═══════════════╬═════════════════════╝
                      ║                                   ║
                      ║       ┌───┐    BCC                ║
                      ║    PM │TMU│            P,M,J-C    ║
                      ║       └───┘                       ║
                      ║                                   ║
                      ║                                   ║
                      ║                                   ║
                      ║  JL                               ║
              GOND    ║                 ┌───┐    ╭────╮   ║
             ╔╛   ╘═══╝                 │KIO│ SU │Obj2│   ║ 
             ║                          └───┘    ╰────╯   ║
             ╚════════════════════════════════════════════╝
                                   Front Lawn
```

| Label     | Description                                                        |
| --------- | ------------------------------------------------------------------ |
| GOND      | Gondola to/from Staging                                            |
| SA▲       | [Become Yourself](objectives/become_santa.md)                      |
| Obj2      | [Objective 2: Investigate S3 Bucket](objectives/obj2.md)           |
| Obj3      | [Objective 3: Point-of-Sale Password Recovery](objectives/obj3.md) |
| Elevator  | [Objective 4: Operate the Santavator](objectives/obj4.md)          |
| Elevator  | [Objective 10: Defeat Fingerprint Sensor](objectives/obj10.md)     |
| Obj6      | [Objective 6: Splunk Challenge](objectives/obj6.md)                |
| 336       | [33.6kbps](side_missions/33_6kbps.md)                              |
| ELF       | [Elf Code](side_missions/elfcode.md)                               |
| KIO       | [Kringle Kiosk](side_missions/kiosk.md)                            |
| LPR       | [Linux Primer](side_missions/linux.md)                             |
| RED       | [Redis Bug Hunt](side_missions/redis.md)                           |
| TMU       | [Unescape Tmux](side_missions/tmux.md)                             |
| BCC       | Broken Candy Cane                                                  |
| GLB       | Green Light Bulb                                                   |
| HN        | Hex Nut (x2)                                                       |
| AC        | Angel Candysalt                                                    |
| BL        | Bubble Lightington                                                 |
| FS        | Fitzy Shortstack                                                   |
| GB        | Ginger Breddie                                                     |
| HE        | Holly Evergreen                                                    |
| JF        | Jack Frost                                                         |
| JL        | Jewel Loggins                                                      |
| P, M, J-C | Pierre, Marie, and Jean-Claude                                     |
| PM        | Pepper Minstix                                                     |
| PS        | Piney Sappington                                                   |
| RB        | Ribb Bonbowford                                                    |
| SM        | Sugarplum Mary                                                     |
| SR        | Sparkle Redberry                                                   |
| SU        | Shinny Upatree                                                     |

## :fontawesome-solid-hammer: Floor 1.5
```{ .code .map }
                            ╔══════════════════════╗
                            ║   ╭────╮       ┌────┐║
                            ║ NB│Obj8│PM     │    │║ Wrapping Room
                            ║   ╰────╯       └────┘║
                            ║                      ║
                            ║                    BA║
                     ╔══════╩═════════════╛    ╘═══╣
            ╔════════╣         ┌───┐               ║
            ║        ║         │SOM│   MC          ║
            ║        ╜╭────╮   └───┘               ║
            ║  DRC    │Obj5│                       ║
            ║        ╖╰────╯                       ║
            ║        ║    ┌──────────────┐         ║
            ║        ║    │              │         ║
            ║        ║    └──────────────┘         ║
            ║        ║    ┌──────────────┐         ║ Workshop
            ║        ║    │              │ M       ║
            ║        ║    └──────────────┘         ║
            ║  ▼SA▼  ║                             ║
            ╚════════╣                             ║
                     ║┌──────────┐                 ║
                     ║│ Elevator │                 ║
                     ║│          │                 ║
                     ║└──────────┘                 ║
                     ║                             ║
                     ╚═════════════════════════════╝
```

| Label | Description                                                           |
| ----- | --------------------------------------------------------------------- |
| ▼SA▼  | [Become Santa](objectives/become_santa.md)                            |
| Obj5  | [Objective 5: Open HID Lock](objectives/obj5.md)                      |
| Obj8  | [Objective 8: Broken Tag Generator](objectives/obj8.md)               |
| SOM   | [SORT-O-MATIC](side_missions/sortomatic.md)                           |
| DRC   | [Dark Room Challenge](objectives/become_santa.md#dark-room-challenge) |
| M     | Marble                                                                |
| PM    | Proxmark                                                              |
| BA    | Rubber Ball                                                           |
| MC    | Minty Candycane                                                       |
| NB    | Noel Boetie                                                           |

## :fontawesome-solid-comment: Floor 2
```{ .code .map }
                                 T1    T2    T3    T4    T5    T6    T7
╔═════════════════════╦═════════╛  ╘══╛  ╘══╛  ╘══╛  ╘══╛  ╘══╛  ╘══╛  ╘═══╗
║┌───┐        SN      ║┌───┐                                             RB║
║│SBF│ TC             ║│SUP│ BE          JF                          ┌───┐ ║
║└───┘                ╜└───┘                                     CS  │GCG│ ║
║          MN                                                        └───┘ ║
║                     ╖                                                    ║
║                     ║                           BN                       ║
║                  EB ║                                                    ║
╚═════════════════════╩══════════════════════════════════╗                 ║
 Speaker UNPreparedness                  Talks           ║                 ║
                                                         ║     ┌──────────┐║
                                                         ║     │ Elevator │║
                                                         ║     │          │║
                                                         ║     └──────────┘║
                                                         ║                 ║
                                                         ╚═════════════════╝
```

| Label | Description                                                 |
| ----- | ----------------------------------------------------------- |
| SUP   | [Speaker Unpreparedness](side_missions/speaker_rm/intro.md) |
| SBF   | [Snowball Fight](side_missions/snowball.md)                 |
| GCG   | [Greeting Card Generator](side_missions/greeting_card.md)   |
| RB    | Red Light Bulb                                              |
| EB    | Elevator Button for Floor 1.5                               |
| BE    | Bushy Evergreen                                          |
| BN    | Bow Ninecandle                                              |
| CS    | Chimney Scissorsticks                                       |
| JF    | Jack Frost                                                  |
| MN    | Morcel Nougat                                               |

## :fontawesome-solid-scroll: Santa's Office
```{ .code .map }
                        Balcony
               ╔═══════════════════════╗
               ║ES     ╭──────╮        ║
               ║       │ENDING│        ║
               ║       ╰──────╯        ║
               ║                       ║
╔══════════════╩═════════╛   ╘═════════╩══════════════╗
║        ╭─────╮                                      ║
║   ┌────┤Obj11├────┐                                 ║
║   │    ╰─────╯    │                                 ║ Santa's
║   └───────────────┘                                 ║ Office
║               TU                                    ║
║                                                     ║
║                                                     ║
╚═════════════════════╗                          ╔════╝
                      ║                          ║
                      ║              ┌──────────┐║
                      ║              │ Elevator │║
                      ║              │          │║
                      ║              └──────────┘║
                      ║                          ║
                      ╚══════════════════════════╝
```

| Label  | Description                                                                                   |
| ------ | --------------------------------------------------------------------------------------------- |
| ENDING | [The End](objectives/ending.md)                                                               |
| Obj11  | [Objective 11a: Naughty/Nice List with Blockchain Investigation Part 1](objectives/obj11a.md) |
| Obj11  | [Objective 11b: Naughty/Nice List with Blockchain Investigation Part 2](objectives/obj11b.md) |
| ES     | Eve Snowshoes                                                                                 |
| TU     | Tinsel Upatree                                                                                |


## :fontawesome-solid-terminal: Roof/NetWars
```{ .code .map }
╔════════════════════════════════════════════════════════════════╗
║     ╭────╮                                                     ║
║     │Obj7│                                                     ║
║     ╰────╯        ┌───────────────────────────────┐            ║
║                   │                               │            ║
║ YB                └───────────────────────────────┘            ║
║                                                                ║
║ ┌────┐            ┌───────────────────────────────┐            ║
║ │CANB│   WO       │                               │            ║ Roof/
║ └────┘            └───────────────────────────────┘            ║ NetWars
║                                                                ║
║                                                                ║
║                      ╭────╮        ┌────┐          ┌──────────┐║
║                      │Obj9│   AS   │ SP │          │ Elevator │║
║                      ╰────╯        └────┘          │          │║
║               JF                                   └──────────┘║
║                                                                ║
╚════════════════════════════════════════════════════════════════╝
```

| Label | Description                                                             |
| ----- | ----------------------------------------------------------------------- |
| Obj7  | [Objective 7: Solve the Sleigh's CAN-D-BUS Problem](objectives/obj7.md) |
| Obj9  | [Objective 9: ARP Shenanigans](objectives/obj9.md)                      |
| CANB  | [CAN-Bus Investigation](side_missions/can_bus.md)                       |
| SP    | [Scapy Prepper](side_missions/scapy.md)                                 |
| YB    | Yellow Light Bulb                                                       |
| AS    | Alabaster Snowball                                                      |
| JF    | Jack Frost                                                              |
| WO    | Wunorse Openslae                                                        |
