#:fontawesome-solid-check-circle:{: .green-text} Objective 4: Operate the Santavator

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree::fontawesome-solid-tree::fontawesome-solid-tree:

Talk to Pepper Minstix in the entryway to get some hints about the Santavator.

## Hints

> It's really more art than science. The goal is to put the right colored light into the receivers on the left and top of the panel.

## Solution
To be able to power the Santavator, you will need to collect several of the [items](../items.md) from around the castle (or use JavaScript-fu). Which items are required is largely limited by your creativity as there are numerous solutions to this puzzle. Whatever solution you come up with, the overall goal is to direct the correctly colored light into the correct receiver. Accessing the different parts of the castle require sending light to one or more of the receivers, as outlined below:

![](../images/obj4/elevator_legend.png)

To achieve Objective 4 you only need to power the Santavator and visit any other floor, however you will eventually have to be able to access all parts of the castle to finish the game.

## Access Talks Lobby

The [Talks Lobby](/map#floor-2) can be accessed using the following items found on the [ground floor](../map.md#ground-floor):

* Access Panel Key (Entryway)
* Green Light Bulb (Courtyard)
* Hex Nut 2 (Dining Room)

One possible solution using these items is shown below:

![](../images/obj4/green-lit.png)

## Access Roof

The [Roof](../map.md#roofnetwars) can be accessed using the following items found on the [ground floor](../map.md#ground-floor) and in the [second floor](../map.md#floor-2):

* Access Panel Key (Entryway)
* Green Light Bulb (Courtyard)
* Red Light Bulb (Talks Lobby)
* Hex Nut 2 (Dining Room)

One possible solution using these items is shown below:

![](../images/obj4/green-red-lit.png)

## Access Santa's Office

[Santa's Office](../map.md#santas-office) can be accessed using the following items found on the [ground floor](../map.md#ground-floor), the [second floor](../map.md#floor-2), and the [roof](../map.md#roofnetwars):

* Access Panel Key (Entryway)
* Green Light Bulb (Courtyard)
* Red Light Bulb (Talks Lobby)
* Yellow Light Bulb (Roof)
* Hex Nut 2 (Dining Room)
* Portals (Speaker UNpreparedness Room, Vending Machine)

You will also need to [be Santa](become_santa.md) or [bypass the fingerprint scanner](obj10.md)

One possible solution using these items is shown below:

![](../images/obj4/all-lit.png)
