# 🎅 Become Santa

## Dark Room Challenge
After bypassing the HID lock in the Workshop, you will be in a dark room. You will need to "feel" your way around the room to get to the opposite end. The map below gives you the route through the room:
```
╔═╪══╪══╪══╪══╪═╗
╫             X ╫        D2 
╫             │ ╫        L2  
╫       ┌─────┘ ╫        D1
╫    ┌──┘       ╫        L1
╫    │          ╫        D2
╫ ┌──┘          ╫        L1
╫ │             ╫        D3
╫ │             ╫        R1
╫ └──┐          ╫        D2
╫    │          ╫        R1
╫    └──┐       ╫        D1
╫       └─────┐ ╫        R2
╫             │ ╫        D2
╫       ┌─────┘ ╫        L2
╫       X       ╫        D2
╚═╪══╪══╪══╪══╪═╝
```

At the opposite end of the room are two eye holes, and if you walk up to them, you suddenly become Santa and are back in the entryway. 

To turn back into yourself, walk towards the Santa painting in the entryway ([SA▲ on the map](../map.md#ground-floor)) and you will come back out in the dark room as yourself.
