#:fontawesome-solid-check-circle:{: .green-text} Objective 6: Splunk Challenge

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree::fontawesome-solid-tree:

Access the Splunk terminal in the Great Room. What is the name of the adversary group that Santa feared would attack KringleCon?

**TL;DR - [Answer](#answer)**

## Hints

> Defenders often need to manipulate data to decRypt, deCode, and refourm it into something that is useful. Cyber Chef is extremely useful here!

> There was a great [Splunk talk](https://www.youtube.com/watch?v=qbIhHhRKQCw) at KringleCon 2 that's still available!

> Dave Herrald talks about emulating advanced adversaries and [hunting them with Splunk](https://www.youtube.com/watch?v=RxVgEFt08kU).

## Training Questions

**To access the Splunk terminal you must [be Santa](become_santa.md).**

When you access the Splunk terminal you will be in a chat with Alice Bluebird. She will give you hints to help you through the Training Questions and the Challenge Question. To execute Splunk queries, you will need to open the search console via the link in the upper-left corner of the page.

> How many distinct MITRE ATT&CK techniques did Alice emulate?

`| tstats count where index=* by index`

* This query will show all the indices in Splunk. From there we can count all of the unique T#### numbers in the list, excluding the sub-techniques.

**13**

> What are the names of the two indexes that contain the results of emulating Enterprise ATT&CK technique 1059.003? (Put them in alphabetical order and separate them with a space)

* From the same list above we can see the two indices for the T1059.003 sub-technique.

**`t1059.003-main t1059.003-win`**

> One technique that Santa had us simulate deals with 'system information discovery'. What is the full name of the registry key that is queried to determine the MachineGuid?

* A search of the Atomic Red Team [GitHub repo](https://github.com/redcanaryco/atomic-red-team) for `MachineGUID` finds the [file detailing with this technique](https://github.com/redcanaryco/atomic-red-team/blob/master/atomics/T1082/T1082.md). In the file we find the appropriate registry key:

**`HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography`**

> According to events recorded by the Splunk Attack Range, when was the first OSTAP related atomic test executed? (Please provide the alphanumeric UTC timestamp.)


* For this question we can use the query `index=attack OSTAP` to find the OSTAP-related events in the `attack` index. The oldest item in the list has the execution time of:

**2020-11-30T17:44:15Z**

> One Atomic Red Team test executed by the Attack Range makes use of an open source package authored by frgnca on GitHub. According to Sysmon (Event Code 1) events in Splunk, what was the ProcessId associated with the first use of this component?

* A Google search for `frgnca github` leads to the user's GitHub profile page. The author has 8 repos, but only one related to [Windows tools](https://github.com/frgnca/AudioDeviceCmdlets). These cmdlets allow an attacker to control audio devices on Windows, so let's search the Atomic Red Team repo for `audio`. This search leads to [T1123](https://github.com/redcanaryco/atomic-red-team/blob/master/atomics/T1123/T1123.md). With this knowledge we can search Splunk for events related to this technique and the relevant EventCode: `index="t1123-win" WindowsAudioDevice EventCode=1`. This returns two events - submit the PID of the earlier event:

**3648**

> Alice ran a simulation of an attacker abusing Windows registry run keys. This technique leveraged a multi-line batch file that was also used by a few other techniques. What is the final command of this multi-line batch file used as part of this simulation?

* A search of the Atomic Red Team repo for `run key` leads to sub-technique [T1547.001](https://github.com/redcanaryco/atomic-red-team/blob/master/atomics/T1547.001/T1547.001.md). A search of this page for "bat" finds two possible files: `batstartup.bat` and `Discovery.bat`. Viewing these files in the repo shows that only [`Discovery.bat`](https://github.com/redcanaryco/atomic-red-team/blob/master/ARTifacts/Misc/Discovery.bat) is multi-line, and the last line is:

**quser**

> According to x509 certificate events captured by Zeek (formerly Bro), what is the serial number of the TLS certificate assigned to the Windows domain controller in the attack range?

* If we search Splunk with the query `index=* sourcetype=bro* sourcetype="bro:x509:json" certificate.subject="*dc*"` we see the same `certificate.serial` many times:

**55FCEEBB21270D9249E86F4B9DC7AA60**

## Challenge Question

After completing all of the training questions, Alice gives us the final clue for the challenge question:

> This last one is encrypted using your favorite phrase! The base64 encoded ciphertext is: `7FXjP1lyfKbyDK/MChyf36h7`

> It's encrypted with an old algorithm that uses a key. We don't care about RFC 7465 up here! I leave it to the elves to determine which one!

Since [RFC7465](https://tools.ietf.org/html/rfc7465) is titled "Prohibiting RC4 Cipher Suites", we let's try decrypting the ciphertext with RC4. But for RC4 we need a key: luckily Alice gives us a hint on this too:

> I can't believe the Splunk folks put it in their talk!

At the end of the YouTube video of this year's [Splunk talk](https://youtu.be/RxVgEFt08kU?t=1111) is the key we need: `Stay Frosty`. We can do the decoding and encrypting in one step with [CyberChef](https://gchq.github.io/CyberChef)

![](../images/obj6/cyberchef.png)

## Answer

**The Lollipop Guild**
