#:fontawesome-solid-check-circle:{: .green-text} Objective 1: Uncover Santa's Gift List 

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree::fontawesome-solid-tree::fontawesome-solid-tree::fontawesome-solid-tree:

There is a photo of Santa's Desk on that billboard with his personal gift list. What gift is Santa planning on getting Josh Wright for the holidays? Talk to Jingle Ringford at the bottom of the mountain for advice.

**TL;DR - [Answer](#answer)**

## Hints

> There are [tools](https://www.photopea.com/) out there that could help Filter the Distortion that is this Twirl.

> Make sure you Lasso the correct twirly area.

## Solution

Let's see what Jingle Ringford has to say:
> Oh, and before you head off up the mountain, you might want to try to figure out what's written on that advertising bilboard.

> Have you managed to read the gift list at the center?

> It can be hard when things are twirly. There are tools that can help!

> It also helps to select the correct twirly area.

The billboard Jingle is referring to is at the upper-left of the screen. Clicking on it opens the following PNG:
![Original billboard](../images/obj1/billboard.png)

From the image we can see that Santa's gift list has been obfuscated with a twirl filter. Since this kind of filter is non-destructive, twirling in the opposite direction should allow us to read the original text. Using a photo editor such as Photopea we must first select the area to distort. The hints suggest using the Lasso Select, but the Rectangular Select ![](../images/obj1/select.png) seems to be easier to get usable results. Select the twirled area of the image, attempting to center the selection on the middle of the twirled area. With the selection made, select Filter -> Distort -> Twirl from the menu. Play with the slider until you have readable text for Josh Wright's line of the list. If done correctly, the results will look similar to this:
![Twirled billboard](../images/obj1/billboard-twisted.png)

From the deobfuscated image, we see that Santa was planning on getting Josh Wright a [**Proxmark**](https://proxmark.com/) for Christmas.

## Answer

**Proxmark**
