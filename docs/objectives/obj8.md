#:fontawesome-solid-check-circle:{: .green-text} Objective 8: Broken Tag Generator

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:

Help Noel Boetie fix the [Tag Generator](https://tag-generator.kringlecastle.com/) in the Wrapping Room. What value is in the environment variable `GREETZ`? Talk to Holly Evergreen in the kitchen for help with this.

**TL;DR - [Answer](#answer)**

## Hints

> If you find a way to execute code blindly, I bet you can redirect to a file then download that file!

> We might be able to find the problem if we can get source code!

> Once you know the path to the file, we need a way to download it!

> Can you figure out the path to the script? It's probably on error pages!

> If you're having trouble seeing the code, watch out for the Content-Type! Your browser might be trying to help (badly)!

> Is there an endpoint that will print arbitrary files?

> I'm sure there's a vulnerability in the source somewhere... surely Jack wouldn't leave their mark?

> Remember, the processing happens in the background so you might need to wait a bit after exploiting but before grabbing the output!

## Solution

**To access the tag generator terminal you must [be Santa](become_santa.md).**

Noel Boetie tells us:
> Welcome to the Wrapping Room, Santa!	

> The tag generator is acting up.

> I feel like the issue has something to do with weird files being uploaded.

> Can you help me figure out what's wrong?

When exploring a web application for vulnerabilities, a good first step is to direct your web browser traffic through a proxy such as Burp Suite to allow you to view and manipulate the underlying requests the browser makes. With our traffic proxied, we upload a file and look at the requests that were made. There are two requests of interest: when we add an image to the web app, our browser makes a POST request to `/upload` with the file contents. The application responds with a JSON list of the filenames of the files that were uploaded.

![POST to /upload](../images/obj8/upload.png)

The next request is a GET request to `/image?id=...` where the `id` parameter is the filename we received from the POST request. If you do not see this request in Burp, you may need to modify your filter settings to show image files.

![GET /image](../images/obj8/get_image.png)

Let's right-click and send this request to Repeater. We will use Repeater to try and retrieve files that we shouldn't have access to. For example, let's try using a directory traversal string to access `/etc/passwd`

![GET /etc/passwd](../images/obj8/etc_passwd.png)

As you can see, this lack of path sanitization has allowed a [local file inclusion (LFI) vulnerability](https://en.wikipedia.org/wiki/File_inclusion_vulnerability#Local_file_inclusion). At this point we could use the LFI to retrieve the application source code and look for other vulnerabilities, but that is unnecessary in this case - we merely need to access the environment variables of the app's process to get to the `GREETZ` variable. By viewing the man page of [`proc`](https://man7.org/linux/man-pages/man5/proc.5.html) we find that the initial environment variables of a given process are in `/proc/[pid]/environ`, but we do not know the `pid` of the running application. Luckily, the man page also describes a magic symbolic link at `/proc/self`. When a process accesses this file, it resolves to the process's own `/proc/[pid]` directory. Thus, if we use the LFI to access `/proc/self/environ` we will be able to see the environment variables for the web application.

![GET /proc/self/environ](../images/obj8/environ.png)

## Answer

**JackFrostWasHere**
