# :fontawesome-solid-gift: The End
After completing [Objective 11b](obj11b.md), go to the balcony behind [Santa's Office](../map.md#santas-office) as yourself and you will see the final scene of the game.

![](../images/ending.png)

Eve Snowshoes

> What a fantabulous job! Congratulations!

> You MUST let us know how you did it!

> Feel free to show off your skills with some swag (link redacted) - only for our victors!

Santa

> Thank you for foiling Jack’s foul plot!

> He sent that magical portrait so he could become me and destroy the holidays!

> Due to your incredible work, you have set everything right and saved the holiday season!

> Congratulations on a job well done!

> Ho Ho Ho!

Jack Frost

> My plan was NEARLY perfect… but I never expected someone with your skills to come around and ruin my plan for ruining the holidays!

> And now, they’re gonna put me in jail for my deeds.

##:fontawesome-solid-candy-cane: And that's it - we saved Christmas! :fontawesome-solid-sleigh:

Thanks Ed Skoudis and the rest of the team at Counter Hack for a great challenge, and looking forward to see what's in store for next year!
