#:fontawesome-solid-check-circle:{: .green-text} Objective 7: Solve the Sleigh's CAN-D-BUS Problem

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree::fontawesome-solid-tree:

Jack Frost is somehow inserting malicious messages onto the sleigh's CAN-D bus. We need you to exclude the malicious messages and no others to fix the sleigh. Visit the NetWars room on the roof and talk to Wunorse Openslae for hints.

**TL;DR - [Answer](#answer)**

## Hints

> Try filtering out one CAN-ID at a time and create a table of what each might pertain to. What's up with the brakes and doors?

## Solution

**To access the sleigh you must [be Santa](become_santa.md).**

Let's start by filtering out all of the recurring events like we did for [CAN-Bus Investigation](../side_missions/can_bus.md). Here is the set of filters to apply:

| ID  | Operator | Criterion    |
| --- | -------- | ------------ |
| 018 | Equals   | 000000000000 |
| 244 | Equals   | 000000000000 |
| 188 | Equals   | 000000000000 |
| 019 | Equals   | 000000000000 |
| 080 | Equals   | 000000000000 |
| 19B | Equals   | 0000000F2057 |

With the noise out of the way, we need to focus on two subsystems based on the hints: the doors and the brakes. We've already figured out the CAN ID for the doors from the [CAN-Bus Investigation](../side_missions/can_bus.md) is `19B`. As a result, our last filter is likely already filtering out that bad message, and that is the only other message on the bus with that code so the doors are probably good. 

Let's apply the brakes and see the messages that are recorded. If we set the brakes to 50 (0x32), we see messages with the ID of `080` coming up, so `080` is probably the brake subsystem.

![](../images/obj7/brakes.png)

One of the messages in each set contains the position of the brake as expected, but the other has a very large value that doesn't really make sense. These are probably Jack's malicious messages. Let's try applying a filter for values larger than 100 (0x64) and see if that gets rid of these messages. If you apply the filter and try again, you will notice the messages don't go away. This is because the values are signed integers, meaning the `FFFF...` values are actually negative. Modify the filter to be less than 0 and the messages will be gone.

## Answer
At this point let's remove all of our filters except the following

| ID  | Operator | Criterion    |
| --- | -------- | ------------ |
| 19B | Equals   | 0000000F2057 |
| 080 | Less     | 000000000000 |

With that we will get the message "Sleigh deFrosted!", meaning we have successfully filtered out Jack's malicious messages.

![](../images/obj7/win.png)
