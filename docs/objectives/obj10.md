#:fontawesome-solid-check-circle:{: .green-text} Objective 10: Defeat Fingerprint Sensor

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:

Bypass the Santavator fingerprint sensor. Enter Santa's office without Santa's fingerprint.

**TL;DR - [Answer](#answer)**

## Hints

> There may be a way to bypass the Santavator S4 game with the browser console...

## Solution

**This objective must be completed as yourself.**

After powering [all three lights](obj4.md#access-santas-office) in the Santavator, we can explore how to bypass the fingerprint scanner. Place the cover back on the panel, and the light for Santa's office should be lit. Selecting ++3++ opens the fingerprint scanner. In most browsers right-clicking on the fingerprint scanner gives the option to "Inspect" the object. This will open the browser's development tools, and show the fingerprint scanner in the page DOM. From here you can view the Event Listeners that are registered for the object - in Chrome this list is in the "Event Listeners" tab. For the fingerprint scanner there are two listeners registered for the `click` event:

![](../images/obj10/listeners.png)

`christmasmagic.js` is a minified JavaScript file, so let's revisit that one if needed. Instead, let's look at `app.js`, line 353.

```javascript hl_lines="6" linenums="348"
const handleBtn4 = () => {
  const cover = document.querySelector('.print-cover');
  cover.classList.add('open');

  cover.addEventListener('click', () => {
    if (btn4.classList.contains('powered') && hasToken('besanta')) {
      $.ajax({
        type: 'POST',
        url: POST_URL,
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ 
          targetFloor: '3',
          id: getParams.id,
        }),
        success: (res, status) => {
          if (res.hash) {
            __POST_RESULTS__({
              resourceId: getParams.id || '1111',
              hash: res.hash,
              action: 'goToFloor-3',
            });
          }
        }
      });
    } else {
      __SEND_MSG__({
        type: 'sfx',
        filename: 'error.mp3',
      });
    }
  });
};

```

We see that the listener checks that the button is powered (check) and calls the `hasToken` function with the string `besanta`. The function `hasToken` is fairly simple: it checks if the `tokens` array contains the string it is was passed as an argument.

```javascript
const hasToken = name => tokens.indexOf(name) !== -1;
```
So, let's add the string `besanta` to our `tokens` array. In the Dev console execute `tokens.push("besanta")`. When you click the fingerprint scanner, you will visit Santa's office as yourself!


## Answer
**Execute `tokens.push("besanta")` in the Dev console of your browser, then click fingerprint scanner**
