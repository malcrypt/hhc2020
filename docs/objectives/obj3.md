#:fontawesome-solid-check-circle:{: .green-text} Objective 3: Point-of-Sale Password Recovery

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree::fontawesome-solid-tree::fontawesome-solid-tree::fontawesome-solid-tree:

Help Sugarplum Mary in the Courtyard find the supervisor password for the point-of-sale terminal. What's the password?

**TL;DR - [Answer](#answer)**

## Hints

> It's possible to extract the source code from an [Electron](https://www.electronjs.org/) app.

> There are [tools](https://www.npmjs.com/package/asar) and [guides](https://medium.com/how-to-electron/how-to-get-source-code-of-any-electron-application-cbb5c7726c37) explaining how to extract ASAR from Electron apps.

## Solution

Let's see what Sugarplum Mary has to say:
> Hey, wouldja' mind helping me get into my point-of-sale terminal?

> It's down, and we kinda' need it running.

> Problem is: it is asking for a password. I never set one!

> Can you help me figure out what it is so I can get set up?

> Shinny says this might be an Electron application.

> I hear there's a way to extract an ASAR file from the binary, but I haven't looked into it yet.

If we try to access the terminal, we are met with this screen:

![](../images/obj3/terminal_locked.png)

We download this [offline version](https://gitlab.com/malcrypt/hhc2020/-/blob/master/pos_password/santa-shop.exe) for inspection, and discover that it is a Windows executable installer, specifically a Nullsoft Scriptable Install System (NSIS) installer. At this point we could run the installer on a Windows machine, but instead we will use 7zip to analyze and extract the contents of the installer.

!!! note
    If the `7z` command is not found, on Debian-based system it can be installed with `sudo apt install p7zip-full`

```console hl_lines="12"
$ 7z l santa-shop.exe 

<-clipped->

   Date      Time    Attr         Size   Compressed  Name
------------------- ----- ------------ ------------  ------------------------
                    .....                      6931  $PLUGINSDIR/System.dll
                    .....                     45608  $PLUGINSDIR/StdUtils.dll
                    .....                      4615  $PLUGINSDIR/SpiderBanner.dll
                    .....                      2027  $PLUGINSDIR/nsProcess.dll
                    .....                      3299  $PLUGINSDIR/nsExec.dll
2020-12-04 12:47:24 .....     49323645     49323645  $PLUGINSDIR/app-64.7z
                    .....                    242382  $PLUGINSDIR/nsis7z.dll
2020-12-04 12:47:26 .....                    113298  Uninstall santa-shop.exe
                    .....                      1080  $PLUGINSDIR/WinShell.dll
------------------- ----- ------------ ------------  ------------------------
2020-12-04 12:47:26           49323645     49742885  9 files
```

Let's extract the `7z` file to see what it contains. We're given a hint about ASAR files, so let's focus on those.

```shell
$ 7z e santa-shop.exe "app-64.7z" -r
$ 7z l app-64.7z| grep "asar"
                    ....A       136143       115548  resources/app.asar
$ 7z x "app-64.7z" "resources/app.asar"
cd resources
```
At this point we need a tool to extract files from [app.asar](https://gitlab.com/malcrypt/hhc2020/-/blob/master/pos_password/resources/app.asar). The hints suggest we look at [https://www.npmjs.com/package/asar](https://www.npmjs.com/package/asar), so let's install `node` and this package. `asar` requires `node` v10+, so the version that is in many repos will be too old. On a Debian-based system:

```shell
wget https://nodejs.org/dist/v14.15.2/node-v14.15.2-linux-x64.tar.xz
tar xf node-v14.15.2-linux-x64.tar.xz
cd node-v14.15.2-linux-x64/bin
./npm install asar

```

!!! warning
    Since we did not setup `PATH` variables when installing `node`, you will need to execute `node` and `asar` via relative paths, and pass in the ASAR file either relative to your current location, or as an absolute path

We can use the `asar` package to list the contents of the file with `list`, and extract a file with `extract-file`. Of particular interest is [`main.js`](https://gitlab.com/malcrypt/hhc2020/-/blob/master/pos_password/main.js) as the password is at the top of the file:

```console hl_lines="7"
$ ./node_modules/asar/bin/asar.js extract-file ASAR_FILE main.js
$ head main.js
// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');

const SANTA_PASSWORD = 'santapass';

...
```

## Answer

**santapass**
