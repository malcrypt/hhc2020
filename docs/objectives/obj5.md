#:fontawesome-solid-check-circle:{: .green-text} Objective 5: Open HID Lock

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree::fontawesome-solid-tree::fontawesome-solid-tree:

Open the HID lock in the Workshop. Talk to Bushy Evergreen near the talk tracks for hints on this challenge. You may also visit Fitzy Shortstack in the kitchen for tips.

**TL;DR - [Answer](#answer)**

## Hints

> Larry Pesce knows a thing or two about [HID attacks](https://www.youtube.com/watch?v=647U85Phxgo). He's the author of a course on wireless hacking!

> There's a [short list of essential Proxmark commands](https://gist.github.com/joswr1ght/efdb669d2f3feb018a22650ddc01f5f2) also available.

> The Proxmark is a multi-function RFID device, capable of capturing and replaying RFID events.

> You can also use a Proxmark to impersonate a badge to unlock a door, if the badge you impersonate has access. `lf hid sim -r 2006......`

> You can use a Proxmark to capture the facility code and ID value of HID ProxCard badge by running `lf hid read` when you are close enough to someone with a badge.

## Solution

Bushy Evergreen says:
> Santa asked me to ask you to evaluate the security of our new HID lock.	

> If ever you find yourself in posession of a Proxmark3, click it in your badge to interact with it.

> It's a slick device that can read others' badges!

> ...

> Oh, did I mention that the Proxmark can simulate badges? Cool, huh?

> There are lots of references online to help.

> In fact, there's [a talk](https://www.youtube.com/watch?v=647U85Phxgo) going on right now!

> ...

> And that Proxmark thing? Some people scan other people's badges and try those codes at locked doors.

> Other people scan one or two and just try to vary room numbers.

> Do whatever works best for you!

Let's try scanning every character in the castle for a badge. To scan for a HID badge, we click on our badge, go to Items, then open the Proxmark console. In the console we execute the command `lf hid read`, and if there is a badge in range, the facility code and the badge number will be displayed. Six badges can be scanned throughout the castle:

| Name               | Location        | Facility Code | Badge #  | Card ID        |
| ------------------ | --------------- | ------------- | -------- | -------------- |
| Angle Candysalt    | Great Room      | 113           | 6040     | 2006e22f31     |
| **Shinny Upatree** | **Front Lawn**  | **113**       | **6025** | **2006e22f13** |
| Sparkle Redberry   | Entryway        | 113           | 6022     | 2006e22f0d     |
| Holly Evergreen    | Kitchen         | 113           | 6024     | 2006e22f10     |
| Noel Boetie        | Wrapping Room   | 113           | 6020     | 2006e22f08     |
| **Bow Ninecandle** | **Talks Lobby** | **113**       | **6023** | **2006e22f0e** |

With the list of known HID IDs, we can go to the HID door on the left-side of the Workshop and use the Proxmark to emulate each badge until one of them works. To emulate a badge we use the command `lf hid sim -r CARDID` in the Proxmark console. In the list above, Shinny and Bow's badges allow us access through the door.

## Answer

**Open the Proxmark console near the HID door, and execute `lf hid sim -r 2006e22f13`**
