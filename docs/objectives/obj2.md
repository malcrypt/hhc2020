#:fontawesome-solid-check-circle:{: .green-text} Objective 2: Investigate S3 Bucket

*Difficulty:* :fontawesome-solid-tree:{: .red-text}:fontawesome-solid-tree::fontawesome-solid-tree::fontawesome-solid-tree::fontawesome-solid-tree:

When you unwrap the over-wrapped file, what text string is inside the package? Talk to Shinny Upatree in front of the castle for hints on this challenge.

**TL;DR - [Answer](#answer)**

## Hints

> It seems like there's a new story every week about data exposed through unprotected [Amazon S3 buckets](https://www.computerweekly.com/news/252491842/Leaky-AWS-S3-bucket-once-again-at-centre-of-data-breach).

> Robin Wood wrote up a guide about [finding these open S3 buckets](https://digi.ninja/blog/whats_in_amazons_buckets.php).

> Santa's Wrapper3000 is pretty buggy. It uses several compression tools, binary to ASCII conversion, and other tools to wrap packages.

>Find Santa's package file from the cloud storage provider. Check Josh Wright's [talk](https://www.youtube.com/watch?v=t4UzXx5JHk0) for more tips!

## Solution

Let's see what Shinny Upatree has to say:
> Say, we've been having an issue with an Amazon S3 bucket.

> Do you think you could help find Santa's `package` file?
 
> Jeepers, it seems there's always a leaky bucket in the news. You'd think we could find our own files!

> Digininja has a great guide, if you're new to S3 searching.

> He even released a tool for the task - what a guy!

> The package wrapper Santa used is reversible, but it may take you some trying.

> Good luck, and thanks for pitching in!

Accessing the terminal to the right of Shinny Upatree we see a `TIPS` file, and a directory containing the [`bucket_finder`](https://digi.ninja/projects/bucket_finder.php) tool. When working with a new tool, it usually helps to [RTFM](https://en.wiktionary.org/wiki/RTFM). So let's execute `bucket_finder` with the `--help` argument.
```console
elf@e22915676357:~/bucket_finder$ ./bucket_finder.rb --help
bucket_finder 1.0 Robin Wood (robin@digininja.org) (www.digininja.org)

Usage: bucket_finder [OPTION] ... wordlist
        --help, -h: show help
        --download, -d: download the files
        --log-file, -l: filename to log output to
        --region, -r: the region to use, options are:
                                        us - US Standard
                                        ie - Ireland
                                        nc - Northern California
                                        si - Singapore
                                        to - Tokyo
        -v: verbose

        wordlist: the wordlist to use
```

There are two arguments we will care about in here: `wordlist` will be the names of the buckets we will try, and `--download` will download the files from any buckets the tool manages to find. Conveniently, we're provided with a `wordlist`. Let's see what it contains:
```console
elf@e22915676357:~/bucket_finder$ cat wordlist
kringlecastle
wrapper
santa
```

When performing dictionary attacks (passwords, directories, etc) a good wordlist is what will ultimately determine if we are successful or not. In this case, there's an obvious omission from the wordlist. Let's add "wrapper3000", the name of the project as Shinny tells us.
```console
elf@e22915676357:~/bucket_finder$ echo "wrapper3000" >> wordlist
```

With the modified `wordlist`, let's go hunting for S3 buckets!
```console
elf@e22915676357:~/bucket_finder$ bucket_finder.rb wordlist --download
http://s3.amazonaws.com/kringlecastle
Bucket found but access denied: kringlecastle
http://s3.amazonaws.com/wrapper
Bucket found but access denied: wrapper
http://s3.amazonaws.com/santa
Bucket santa redirects to: santa.s3.amazonaws.com
http://santa.s3.amazonaws.com/
        Bucket found but access denied: santa
http://s3.amazonaws.com/wrapper3000
Bucket Found: wrapper3000 ( http://s3.amazonaws.com/wrapper3000 )
        <Downloaded> http://s3.amazonaws.com/wrapper3000/package
```

Looks like "wrapper3000" was a valid bucket, and the file [`package`](https://gitlab.com/malcrypt/hhc2020/-/blob/master/s3_bucket/package) was downloaded into the `wrapper3000` directory. If we change to the directory, let's start figuring out how to unwrap this file:
```console
elf@e22915676357:~/bucket_finder$ cd wrapper3000/
elf@e22915676357:~/bucket_finder/wrapper3000$ ls
package
elf@e22915676357:~/bucket_finder/wrapper3000$ file package 
package: ASCII text, with very long lines
elf@e22915676357:~/bucket_finder/wrapper3000$ cat package
UEsDBAoAAAAAAIAwhFEbRT8anwEAAJ8BAAAcABwAcGFja2FnZS50eHQuWi54ei54eGQudGFyLmJ6MlVUCQADoBfKX6AXyl91eAsAAQT2AQAABBQAAABCWmg5MUFZJlNZ2ktivwABHv+Q3hASgGSn//AvBxDwf/xe0gQAAAgwAVmkYRTKe1PVM9U0ekMg2poAAAGgPUPUGqehhCMSgaBoAD1NNAAAAyEmJpR5QGg0bSPU/VA0eo9IaHqBkxw2YZK2NUASOegDIzwMXMHBCFACgIEvQ2Jrg8V50tDjh61Pt3Q8CmgpFFunc1Ipui+SqsYB04M/gWKKc0Vs2DXkzeJmiktINqjo3JjKAA4dLgLtPN15oADLe80tnfLGXhIWaJMiEeSX992uxodRJ6EAzIFzqSbWtnNqCTEDML9AK7HHSzyyBYKwCFBVJh17T636a6YgyjX0eE0IsCbjcBkRPgkKz6q0okb1sWicMaky2Mgsqw2nUm5ayPHUeIktnBIvkiUWxYEiRs5nFOM8MTk8SitV7lcxOKst2QedSxZ851ceDQexsLsJ3C89Z/gQ6Xn6KBKqFsKyTkaqO+1FgmImtHKoJkMctd2B9JkcwvMr+hWIEcIQjAZGhSKYNPxHJFqJ3t32Vjgn/OGdQJiIHv4u5IpwoSG0lsV+UEsBAh4DCgAAAAAAgDCEURtFPxqfAQAAnwEAABwAGAAAAAAAAAAAAKSBAAAAAHBhY2thZ2UudHh0LloueHoueHhkLnRhci5iejJVVAUAA6AXyl91eAsAAQT2AQAABBQAAABQSwUGAAAAAAEAAQBiAAAA9QEAAAAA
```

The contents of `package` looks like Base64. Let's decode it and see what comes out:
```console
elf@e22915676357:~/bucket_finder/wrapper3000$ base64 -d package > package_decoded
elf@e22915676357:~/bucket_finder/wrapper3000$ file package_decoded 
package_decoded: Zip archive data, at least v1.0 to extract
```

The next layer appears to be a zip. Let's look inside the zip:
```console
elf@e22915676357:~/bucket_finder/wrapper3000$ unzip -l package_decoded 
Archive:  package_decoded
  Length      Date    Time    Name
---------  ---------- -----   ----
      415  2020-12-04 11:04   package.txt.Z.xz.xxd.tar.bz2
---------                     -------
      415                     1 file
```

If the naming of the inside file is correct, `package.txt` was compressed with `compress`, compressed with `xz`, hex dumped through `xxd`, packaged with `tar`, then compressed again with `bzip2`. Let's unpack this with one long piped command then view the contents of `package.txt`:
```console
elf@e22915676357:~/bucket_finder/wrapper3000$ unzip -p package_decoded | bunzip2 | tar -xO | xxd -r - | xz -cd | uncompress -c > package.txt
elf@e22915676357:~/bucket_finder/wrapper3000$ cat package.txt 
North Pole: The Frostiest Place on Earth
```

## Answer

**North Pole: The Frostiest Place on Earth**
